## git操作命令

#### 基本操作命令：

git init

git remote add <u>**origin**</u> git地址

git add .

git commit -m "提交备注信息"

git push -u <u>**origin**</u> master   (origin和上面关联远程地址时需要保持一致)

`git reflog` 命令会显示你的本地仓库中的操作记录

#### 查看分支：

查看所有分支（包括远程和本地所有并会显示自己当前分支）：git branch -a

查看所有本地分支（会显示自己当前分支）：git branch 

查看所有远程分支：git branch -r

查看本地修改记录：git status

查看本地分支和远程分支的关联关系：git branch -vv

#### 切换分支：

切换到指定分支：git checkout 分支名称

新建分支并切换：git branch -b 新分支名称

推送本地分支到远程分支（根据本地分支名称新建远程分支）：git push --set-upstream origin 分支名称

本地分支与远程分支进行关联：git branch --set-upstream-to=origin/远程分支  本地分支

创建远程分支到本地：git checkout -b dev origin/dev 

取消关联本地和远程分支：git remote remove origin

#### 合并分支：

合并A分支与B分支(B代码到A)：git checkout A;  			git merge B;

使用merge（无冲突）：merge后 会提示如下图 意思 需要提交一次 请输入提交信息 按i输入 wq保存退出 然后push即可

![在这里插入图片描述](D:\学习资料\md笔记\imgs\git操作命令\3688bc898e784335b38aa6fffba541c5.png)

使用rabase（无冲突）：rebase后 会提示如下信息 无需任何操作了 直接push即可

使用merge（有冲突）：merge后会提示如下图 意思为存在冲突 需要解决冲突 然后 进行 add commit 之后再push

![在这里插入图片描述](D:\学习资料\md笔记\imgs\git操作命令\de37515f03ad4d5182533b0aca6fa85c.png)

使用rebase（有冲突）：rebase后会提示如下图 意思为存在冲突 其中需要解决冲突 然后add continue（注意不需要commit了） 之后再push：

![在这里插入图片描述](D:\学习资料\md笔记\imgs\git操作命令\b708fe98f5da4c39be7baab30b99630a.png)

rebase 过程继续执行 ：git rebase --continue; 

发生代码冲突后，放弃合并，回到操作前的样子：git rebase --abort;

#### 删除分支：

删除本地分支：git branch -d 分支名称

#### 回退与回滚：

查看历史：git log

放弃本地指定文件修改内容（未add）：git checkout --文件路径及名称   

放弃本地所有文件修改内容（未add）：git checkout .

放弃本地指定文件修改内容（已add）：git reset HEAD 文件路径名称

放弃本地所有文件修改内容（已add）：git reset HEAD .

回滚到上一个提交版本：git reset --hard HEAD^

回滚到指定版本：git reset --hard 版本号

#### 上传与下拉：

上传（本地分支A与远程分支A未关联过）：git push -u <u>**origin**</u> A

上传（本地分支A与远程分支A已关联过）：git push  A

**git push**的一般形式为 **git push <远程主机名> <本地分支名> <远程分支名>** 。

例如：git push origin myComputer:myComputer

即是将本地的myComputer分支推送到远程主机origin上的对应myComputer分支。origin 是远程主机名，第一个myComputer是本地分支名，第二个myComputer是远程分支名。

下拉：git pull origin master 	(git pull 等于 git fetch + git merge)

rabase合并远程分支至本地：git pull --rebase origin master rebase方式合并远端分支至本地

#### 解决冲突流程：

明确合并时哪些文件发生冲突 （当前分支为dev1）
执行命令：git merge 分支名 有冲突时会提示哪些文件有冲突
代码冲突：会停留在MERGING状态

![img](D:\学习资料\md笔记\imgs\git操作命令\027a4eb79357bb29fa2cb3db1b1c64d1.png)


2. 查看不同分支代码的差异化

执行命令：cat 冲突文件

![img](D:\学习资料\md笔记\imgs\git操作命令\5d077fb6d1a2353225f26080e0ce68c4.png)

![img](D:\学习资料\md笔记\imgs\git操作命令\a7be1c257e0518001d5cf5656b6bbe99.png)


3. 修改冲突文件（合并代码）

执行命令：vim 冲突文件

![img](D:\学习资料\md笔记\imgs\git操作命令\59bf7df597648bc06ef3232c4bc82ccc.png)

![img](D:\学习资料\md笔记\imgs\git操作命令\11c2ece10b79e96fa315c0f5ad6dba60.png)


通过vi编辑器，删除冲突文件中不需的内容后：

![img](D:\学习资料\md笔记\imgs\git操作命令\8a476b8675c50998aea5c1f35e8e3acb.png)

![img](D:\学习资料\md笔记\imgs\git操作命令\8d2fae1d55c3317672b740d5e90ca1c2.png)

4. 提交修改后的冲突文件

执行命令：git add 修改后的冲突文件 先添加到暂存区
执行命令：git commit -m '消息' 再提交到本地Git


5. 推送到Git远程仓库

执行命令：git push

#### 远程仓库：

查看远程仓库：git remote

#### merge和rebase的区别：

###### merge命令不会保留merge的分支的commit，rebase会保留所有的commit

![img](D:\学习资料\md笔记\imgs\git操作命令\39f8f18940fb4cfeaa6f6c2c89252c2a.png)

rebase会把你当前分支的 commit 放到公共分支的最后面,所以叫变基。就好像你从公共分支又重新拉出来这个分支一样。
举例:如果你从 master 拉了个feature分支出来,然后你提交了几个 commit,这个时候刚好有人把他开发的东西合并到 master 了,这个时候 master 就比你拉分支的时候多了几个 commit,如果这个时候你 rebase master 的话，就会把你当前的几个 commit，放到那个人 commit 的后面。

merge 会把公共分支和你当前的commit 合并在一起，形成一个新的 commit 提交

处理冲突的方式：

（一股脑）使用merge命令合并分支，解决完冲突，执行git add .和git commit -m'fix conflict'。这个时候会产生一个commit。
（交互式）使用rebase命令合并分支，解决完冲突，执行git add .和git rebase --continue，不会产生额外的commit。这样的好处是，‘干净’，分支上不会有无意义的解决分支的commit；坏处，如果合并的分支中存在多个commit，需要重复处理多次冲突。

#### git stash：

1、git stash
        保存当前的工作区与暂存区的状态，把当前的修改的保存到git 栈，等以后需要的时候再恢复，git stash 这个命令可以多次使用，每次使用都会新加一个stash@{num}，num是编号

2、git stash save '注释'
        作⽤等同于git stash，区别是可以加⼀些注释， 执⾏存储时，添加注释，⽅便查找
3、git stash pop
        默认恢复git栈中最新的一个stash@{num}，建议在git栈中只有一条的时候使用，以免混乱

        注：该命令将堆栈中最新保存的内容删除

4、git stash list

        查看当前stash的所有内容

5、git stash apply
        将堆栈中的内容恢复到当前分支下。这个命令不同于 git stash pop。该命令不会将内容从对堆栈中删除，也就是该命令能够将堆栈的内容多次运用到工作目录，适合用与多个分支的场景

        使用方法：git stash apply stash@{$num}

6、git stash drop 
        从堆栈中移除指定的stash

        使用方法：git stash drop stash@{$num}

7、git stash clear
        移除全部的stash

8、git stash show
     查看堆栈中最新保存的stash和当前⽬录的差异，显⽰做了哪些改动，默认show第一个存储

#### 重新关联新的仓库：

先删除git文件：

然后执行以下命令：

```dockerfile
git init
git remote add origin http://60.30.92.228:40029/education/playedu.git
git add .
git commit -m "初始化"
git push -u origin master
```

