## RabbitMQ

#### 1、导入rabbitmq依赖

 <!--rabbitmq-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>

#### 2.配置文件添加rabbitmq连接信息

```yaml
# spring配置
spring:
  rabbitmq:
    host: 192.168.5.189
    port: 5672
    username: admin
    password: admin
    #虚拟机
    virtual-host: /
    #连接超时时间
    connection-timeout: 1000
    #producer
    #confirmslistener 消息是否投递成功
    #publisher-confirms: true
    #没有队列接收消息时，返回一个状态
    publisher-returns: true
    #true当消息无法被正常送达的时候被返回给生产者，false丢弃
    template:
      mandatory: true
    #consumer
    #简单配置，手动的
    listener:
      simple:
        acknowledge-mode: manual
        #最小的消费者数量
        concurrency: 1
        #最大的消费者数量
        max-concurrency: 5
```

#### 3.消息的生产与消费

##### （1）Direct消息

创建交换机、队列以及使用routingkey将其绑定。

```java

@Configuration
public class DirectRabbitConfig {
    //队列 起名：TestDirectQueue
    @Bean
    public Queue TestDirectQueue() {

        // exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于			durable
        // autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除。
        // durable:是否持久化,默认是false,持久化队列：会被存储在磁盘上，当消息代理重启时仍然存在，暂存队列：			当前连接有效
        //   return new Queue("TestDirectQueue",true,true,false);
        //一般设置一下队列的持久化就好,其余两个就是默认false
        return new Queue("TestDirectQueue",true);
    }

    //Direct交换机 起名：TestDirectExchange
    @Bean
    DirectExchange TestDirectExchange() {
      //  return new DirectExchange("TestDirectExchange",true,true);
        return new DirectExchange("TestDirectExchange",true,false);
    }

    //绑定  将队列和交换机绑定, 并设置用于匹配键：TestDirectRouting
    @Bean
    Binding bindingDirect() {
        return BindingBuilder.bind(TestDirectQueue()).to(TestDirectExchange()).with("TestDirectRouting");
    }
}
```

 发送消息到指定交换机（由于是直连交换机，需要绑定路由key进行发送）

```java
@Autowired
RabbitTemplate rabbitTemplate;  //使用RabbitTemplate,这提供了接收/发送等等方法
 
@GetMapping("/sendDirectMessage")
public String sendDirectMessage() {
    String messageId = String.valueOf(UUID.randomUUID());
    String messageData = "test message, hello!";
    String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    Map<String,Object> map=new HashMap<>();
    map.put("messageId",messageId);
    map.put("messageData",messageData);
    map.put("createTime",createTime);
    //将消息携带绑定键值：TestDirectRouting 发送到交换机TestDirectExchange，其交换机名称和路由key务必和已创建的交换机名和所使用的路由key相同
    rabbitTemplate.convertAndSend("TestDirectExchange", "TestDirectRouting", map);
    return "ok";
}
```

消费者进行监听消费：

```java
@RabbitListener(queues = "TestDirectQueue")
public void quickEqpConfigurationImport(Message msg, Channel channel) {
    System.out.println("收到消息:" + msg);
    try {
        channel.basicAck(msg.getMessageProperties().getDeliveryTag(), true);
    } catch (Exception e) {
        channel.basicReject(deliveryTag, false);
        log.error("消息签收失败：{}",msg);
    }
}
```

##### (2)Topic消息

创建交换机、队列，以及使用不同的路由key进行绑定。

```java
@Configuration
public class TopicRabbitConfig {
    //绑定键
    public final static String man = "topic.man";
    public final static String woman = "topic.woman";
    @Bean
    public Queue firstQueue() {
        return new Queue(TopicRabbitConfig.man);
    }

    @Bean
    public Queue secondQueue() {
        return new Queue(TopicRabbitConfig.woman);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("topicExchange");
    }
    //将firstQueue和topicExchange绑定,而且绑定的键值为topic.man
    //这样只要是消息携带的路由键是topic.man,才会分发到该队列
    @Bean
    Binding bindingExchangeMessage() {
        return BindingBuilder.bind(firstQueue()).to(exchange()).with(man);
    }

    //将secondQueue和topicExchange绑定,而且绑定的键值为用上通配路由键规则topic.#
    // 这样只要是消息携带的路由键是以topic.开头,都会分发到该队列
    @Bean
    Binding bindingExchangeMessage2() {
        return BindingBuilder.bind(secondQueue()).to(exchange()).with("topic.#");
    }
 }
```

分别给两种队列发送消息：

```java
@GetMapping("/sendTopicMessage1")
public String sendTopicMessage1() {
    String messageId = String.valueOf(UUID.randomUUID());
    String messageData = "message: M A N ";
    String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    Map<String, Object> manMap = new HashMap<>();
    manMap.put("messageId", messageId);
    manMap.put("messageData", messageData);
    manMap.put("createTime", createTime);
    rabbitTemplate.convertAndSend("topicExchange", "topic.man", manMap);
    return "ok";
}
 
@GetMapping("/sendTopicMessage2")
public String sendTopicMessage2() {
    String messageId = String.valueOf(UUID.randomUUID());
    String messageData = "message: woman is all ";
    String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    Map<String, Object> womanMap = new HashMap<>();
    womanMap.put("messageId", messageId);
    womanMap.put("messageData", messageData);
    womanMap.put("createTime", createTime);
    rabbitTemplate.convertAndSend("topicExchange", "topic.woman", womanMap);
    return "ok";
}
```

消费者消费消息：

```java
@RabbitListener(queues = "topic.man")
public void quickEqpConfigurationImport(Message msg, Channel channel) {
    System.out.println("man收到消息:" + msg);
    try {
        channel.basicAck(msg.getMessageProperties().getDeliveryTag(), true);
    } catch (Exception e) {
        log.error("消息签收失败：{}",msg);
    }
}

@RabbitListener(queues = "topic.woman")
public void quickEqpConfigurationImport(Message msg, Channel channel) {
    System.out.println("woman收到消息:" + msg);
    try {
        channel.basicAck(msg.getMessageProperties().getDeliveryTag(), true);
    } catch (Exception e) {
        channel.basicReject(deliveryTag, false);
        log.error("消息签收失败：{}",msg);
    }
}
```

调试：调用/sendTopicMessage1接口，两个消费者均可收到消息。

​			调用/sendTopicMessage2接口，只有topic.woman可收到消息。

##### （3）Fanout消息

创建交换机、队列，将交换机和队列绑定（不需要路由key）

```java
@Configuration
public class FanoutRabbitConfig {
    /**
     *  创建三个队列 ：fanout.A   fanout.B  fanout.C
     *  将三个队列都绑定在交换机 fanoutExchange 上
     *  因为是扇型交换机, 路由键无需配置,配置也不起作用
     */
    @Bean
    public Queue queueA() {
        return new Queue("fanout.A");
    }
    @Bean
    public Queue queueB() {
        return new Queue("fanout.B");
    }
    @Bean
    public Queue queueC() {
        return new Queue("fanout.C");
    }
    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanoutExchange");
    }
    @Bean
    Binding bindingExchangeA() {
        return BindingBuilder.bind(queueA()).to(fanoutExchange());
    }
    @Bean
    Binding bindingExchangeB() {
        return BindingBuilder.bind(queueB()).to(fanoutExchange());
    }
    @Bean
    Binding bindingExchangeC() {
        return BindingBuilder.bind(queueC()).to(fanoutExchange());
    }
}
```

发送消息：

```java
@GetMapping("/sendFanoutMessage")
public String sendFanoutMessage() {
    String messageId = String.valueOf(UUID.randomUUID());
    String messageData = "message: testFanoutMessage ";
    String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    Map<String, Object> map = new HashMap<>();
    map.put("messageId", messageId);
    map.put("messageData", messageData);
    map.put("createTime", createTime);
    rabbitTemplate.convertAndSend("fanoutExchange", null, map);
    return "ok";
}
```
消费消息：

```java
@RabbitListener(queues = {"fanout.A","fanout.B","fanout.C"})
public void quickEqpConfigurationImport(Message msg, Channel channel) {
    System.out.println("收到消息:" + msg);
    try {
        channel.basicAck(msg.getMessageProperties().getDeliveryTag(), true);
    } catch (Exception e) {
        channel.basicReject(deliveryTag, false);
        log.error("消息签收失败：{}",msg);
    }
}
```
##### 4.消息确认机制

①自动确认， 这也是默认的消息确认情况。  AcknowledgeMode.NONE
RabbitMQ成功将消息发出（即将消息成功写入TCP Socket）中立即认为本次投递已经被正确处理，不管消费者端是否成功处理本次投递。所以这种情况如果消费端消费逻辑抛出异常，也就是消费端没有处理成功这条消息，那么就相当于丢失了消息。一般这种情况我们都是使用try catch捕捉异常后，打印日志用于追踪数据，这样找出对应数据再做后续处理。

②  手动确认 ， 这个比较关键，也是我们配置接收消息确认机制时，多数选择的模式。
消费者收到消息后，手动调用basic.ack/basic.nack/basic.reject后，RabbitMQ收到这些消息后，才认为本次投递成功。
basic.ack用于肯定确认 
basic.nack用于否定确认（注意：这是AMQP 0-9-1的RabbitMQ扩展） 
basic.reject用于否定确认，但与basic.nack相比有一个限制:一次只能拒绝单条消息 

消费者端以上的3个方法都表示消息已经被正确投递，但是basic.ack表示消息已经被正确处理。

**channel.basicReject(deliveryTag, true);**  拒绝消费当前消息，如果第二参数传入true，就是将数据重新丢回队列里，那么下次还会消费这消息。设置false，就是告诉服务器，我已经知道这条消息数据了，因为一些原因拒绝它，而且服务器也把这个消息丢掉就行。 下次不想再消费这条消息了。
