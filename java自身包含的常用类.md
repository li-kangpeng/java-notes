## java自身包含的常用类

#### 一、变量修饰属性volatile

volatile是Java虚拟机提供的轻量级的同步机制，它有３个特性：
１）保证可见性
２）不保证原子性
３）禁止指令重排

**1、保证可见性**

可见性概念：JMM内存模型的可见性是指，多线程访问主内存的某一个资源时，如果某一个线程在自己的工作内存中修改了该资源，并写回主内存，那么JMM内存模型应该要通知其他线程来从新获取最新的资源，来保证最新资源的可见性。

验证：

```java
    int testVisibilityNum1 = 0;

    volatile int testVisibilityNum2 = 0;

    public int getTestVisibilityNum1() {
        return testVisibilityNum1;
    }

    public void setTestVisibilityNum1(int testVisibilityNum1) {
        this.testVisibilityNum1 = testVisibilityNum1;
    }

    public int getTestVisibilityNum2() {
        return testVisibilityNum2;
    }

    public void setTestVisibilityNum2(int testVisibilityNum2) {
        this.testVisibilityNum2 = testVisibilityNum2;
    }

    /**
     * 保证可见性 多线程访问主内存的某一个资源时，如果某一个线程在自己的工作内存中修改了该资源，并写回主内存，
     * 那么JMM内存模型应该要通知其他线程来从新获取最新的资源，来保证最新资源的可见性
     */
    @Test
    public void testVisibility() throws InterruptedException {
        VolatileTest volatileTest = new VolatileTest();
        new Thread(() -> {
            System.out.println("不带有volatile执行前，结果为"+volatileTest.testVisibilityNum1);
            try{ Thread.sleep(3000);}catch (Exception e){e.printStackTrace();}
            volatileTest.setTestVisibilityNum1(5);
            System.out.println("不带有volatile执行后，结果为"+volatileTest.getTestVisibilityNum1());
        },"线程1").start();
        new Thread(() -> {
            System.out.println("带有volatile执行前，结果为"+volatileTest.testVisibilityNum2);
            try{ Thread.sleep(3000);}catch (Exception e){e.printStackTrace();}
            volatileTest.setTestVisibilityNum2(5);
            System.out.println("带有volatile执行后，结果为"+volatileTest.getTestVisibilityNum2());
        },"线程2").start();
        //使用volatile关键字，在其他线程对变量的修改会返回------没有使用volatile关键字的变量，在其他线程对该变量做出修改时，原有变量不会发生改变
        new Thread(()->{
            while (volatileTest.getTestVisibilityNum1() == 0){ }
            System.out.println("不带有volatile属性的输出为：" + volatileTest.getTestVisibilityNum1() );
        },"线程1监听").start();
        new Thread(()->{
            while (volatileTest.getTestVisibilityNum2() == 0){ }
            System.out.println("带有volatile属性的输出为：" + volatileTest.getTestVisibilityNum2() );
        },"线程2监听").start();
        if(Thread.activeCount()>2)
            Thread.sleep(10*1000);
    }
```

输出：

	不带有volatile执行前，结果为0
	带有volatile执行前，结果为0
	不带有volatile执行后，结果为5
	带有volatile属性的输出为：5
	带有volatile执行后，结果为5
使用volatile关键字，在其他线程对变量的修改会返回。

没有使用volatile关键字的变量，在其他线程对该变量做出修改时，原有变量不会发生改变。

说明volatile具有可见性。

**2、不保证原子性**

原子性概念：原子性是指不可分隔，完整性，即某个线程正在做某个业务时，中间不能被分割。要么同时成功，要么同时失败。

验证：

前提：知道n++编译后的结果会被拆成三个指令执行。

因此可能存在线程１正在执行第１个指令，紧接着线程２也正在执行第１个指令，这样当线程１和线程２都执行完３个指令之后，很容易理解，此时n的值只加了１，而实际是有２个线程加了２次，因此这种情况就是不保证原子性。

![在这里插入图片描述](D:\学习资料\md笔记\imgs\java自身包含的常用类\893442393cf44ea6a890f3c7a6f2d523.png)

```java
volatile  int testAtomicityNum1 = 0;
/**
 * 不保证原子性  前提：n++在编译后会被拆成三个指令执行：1.拿到原始n 2.进行加1操作 3，把累加后的值进行返回
 */
@Test
public void testAtomicity(){
    int threadNum = 5;
    int count = 100;
    for (int i = 0; i < threadNum; i++) {
        new Thread(() -> {
            for (int j = 0; j < count; j++) {
                testAtomicityNum1++;
            }
        }).start();
    }
    // 程序运行时，模型会有主线程和守护线程。如果超过２个，那就说明上面的２０个线程还有没执行完的，就需要等待
    while (Thread.activeCount()>2){
        Thread.yield();
    }
    System.out.println("testAtomicityNum1累计相加"+threadNum*count+"次，结果为"+testAtomicityNum1);
}
```

输出：testAtomicityNum1累计相加500次，结果为494

说明volatile并不保证原子性。

**3、防止指令重排**

指令重排概念：指令重排是指编译器和处理器为了优化程序性能而对指令序列进行重新排序的一种手段，有时候会改变程序语句的先后顺序。不存在数据依赖关系，可以重排序; 存在数据依赖关系，禁止重排序 。但重排后的指令绝对不能改变原有的串行语义。

数据依赖性: 若两个操作访问同一变量，且这两个操作中有一个为写操作，此时两操作间就存在数据依赖性。

如对下面一组语句执行先后情况：

```java
public void mySort() {
    int x = 1;  // 语句1
    int y = 2;  // 语句2
    x = x + 3;  // 语句3
    y = x * x;  // 语句4
}
```

重排序（需考虑数据依赖性）后执行顺序三种：（1）1234	（2）1324	（3）2134

多线程单例模式改进，DCL版存在的问题：

```java
    /**
     * 防止指令重排
     */
    static class Singleton{
        private static Singleton instance = null;
        private Singleton(){
            System.out.println(Thread.currentThread()+"线程初始化执行私有构造方法");
        }
        public static Singleton getInstance() {
            //改进前
//            if (instance == null) {
//                instance = new Singleton();
//            }
            //改进后  DCL版的单例模式依然不是100%准确的
            // DCL(Double Check Lock双端检锁机制)
            if (instance == null) {
                synchronized (Singleton.class) {
                    if (instance == null) {
                        instance = new Singleton();
                    }
                }
            }
            return instance;
        }
    }
    @Test
    public void testRearrangement(){
        for (int i=1; i<=10; i++) {
            new Thread(Singleton::getInstance, String.valueOf(i)).start();
        }
    }
```

DCL版的单例模式依然不是100%准确，原因：

１）分配对象内存空间：memory = allocate();
２）初始化对象：instance(memory);
３）设置instance指向分配的内存地址：instance = memory;

由于步骤２和步骤３不存在数据依赖关系，因此可能出现执行132步骤的情况。
比如线程1执行了步骤13，还没有执行步骤2，此时instance!=null，但是对象还没有初始化完成；
如果此时线程2抢占到cpu，然后发现instance!=null，然后直接返回使用，就会发现instance为空，就会出现异常。
这就是指令重排可能导致的问题，因此要想保证程序100%正确就需要加volatile禁止指令重排。

即

```java
private volatile static Singleton instance = null;
```

<u>volatile保证禁止指令重排的原理：</u>

内存屏障（Memory Barrier）：又称为内存栅栏。它是一个CPU指令，有２个作用：１）保证特定操作的执行顺序；２）保证某些变量的内存可见性；

<u>重排序的分类和执行流程 :</u>

1.编译器优化的重排序:编译器在不改变单线程串行语义的前提下，可以重新调整指令的执行顺序  

2.指令级并行的重排序:处理器使用指令级并行技术来将多条指令重叠执行，若不存在数据依赖性，处理器可以改变语句对应机器指令的执行顺序

3.内存系统的重排序:由于处理器使用缓存和读/写缓冲区，这使得加载和存储操作看上去可能是乱序执行

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\f674fb513f2740f8bfca81ed22093230.png)

<u>volatile有关禁重排的行为：</u>

1. 当第一个操作为volatile读时，不论第二个操作是什么，都不能重排序。这个操作保证了volatile读之后的操作不会被重排到volatile读之前（volatile读之后的操作，都禁止重排序到volatile之前）

2. 当第二个操作为volatile写时，不论第一个操作是什么，都不能重排序。这个操作保证了volatile写之前的操作不会被重排到volatile写之后  (volatile写之前的操作，都禁止重排序到volatile之后)

3. 当第一个操作为volatile写时，第二个操作为volatile读时，不能重排。(volatile写之后volatile读，禁止重排序的)

<u>内存屏障四大指令插入情况</u>

1.在每个volatile写操作的前面插入一个StoreStore屏障，保证在volatile写之前，其前面的所有普通写操作都已经刷新到主内存中。  

2.在每个volatile写操作的后面插入一个StoreLoad屏障，避免volatile写与后面可能有的volatile读/写操作重排序
![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\dcaf8d21f2f0483a95bdde94369670ce.png)

3.在每个volatile**读操作的后面插入一个LoadLoad屏障**，禁止处理器把上面的volatile读与下面的普通读重排序。

4.在每个volatile**读操作的后面插入一个LoadStore屏障**，禁止处理器把上面的volatile读与下面的普通写重排序。

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\226a9a5dc0474dbfb576b30378bf9603.png)



#### 二、ThreadLocal

**使用场景：**

多线程访问同一个共享变量的时候容易出现并发问题，特别是多个线程对一个变量进行写入的时候，为了保证线程安全，一般使用者在访问共享变量的时候需要进行额外的同步措施才能保证线程安全性。ThreadLocal是除了加锁这种同步方式之外的一种保证一种规避多线程访问出现线程不安全的方法，当我们在创建一个变量后，如果每个线程对其进行访问的时候访问的都是线程自己的变量这样就不会存在线程不安全问题。

```java
private ThreadLocal<Integer> threadLocal =  new ThreadLocal<>();

@Test
public void threadLocalTest() throws InterruptedException {
    new Thread(()->{
        threadLocal.set(1);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()+"内的值:"+threadLocal.get());
        threadLocal.remove();
        System.out.println("移除变量"+Thread.currentThread().getName()+"内的值:"+threadLocal.get());
    },"线程1").start();

    new Thread(()->{
        threadLocal.set(2);
        System.out.println(Thread.currentThread().getName()+"内的值:"+threadLocal.get());
        threadLocal.remove();
        System.out.println("移除变量"+Thread.currentThread().getName()+"内的值:"+threadLocal.get());
    },"线程2").start();
    Thread.sleep(1000*5);
}
```

源码：

```java
public void set(T value) {
    Thread t = Thread.currentThread();
    ThreadLocalMap map = getMap(t);
    if (map != null)
        map.set(this, value);
    else
        createMap(t, value);
}
```

ThreadLocal使用线程对象作为key，要设为的值作为value，放入到ThradLocalMap中。

##### InheritableThreadLocal：

```java
/**
 * 可继承线程本地测试
 *
 */
private ThreadLocal<Integer> inheritableThreadLocal = new InheritableThreadLocal<>();

@Test
public void integerInheritableThreadLocalTest() throws InterruptedException {
    threadLocal.set(9999);
    inheritableThreadLocal.set(9999);
    new Thread(()->{
        System.out.println("主线程ThreadLocal："+threadLocal.get());
        System.out.println("主线程inheritableThreadLocal："+inheritableThreadLocal.get());
        //设值设的是当前线程，父线程值不会被更改
        inheritableThreadLocal.set(1);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //当前线程有值则进行访问优先当前线程
        System.out.println(Thread.currentThread().getName()+"内的值:"+inheritableThreadLocal.get());
    },"线程1").start();
    new Thread(()->{
        threadLocal.set(2);
        System.out.println(Thread.currentThread().getName()+"内的值:"+threadLocal.get());
    },"线程2").start();
    Thread.sleep(1000*5);
    System.out.println("主线程"+"内的值:"+threadLocal.get());
}
```

InheritableThreadLocal可以实现父子线程通讯，子线程可以访问父线程ThreadLocal的设置的值。

原理：

Thread类：

```java
ThreadLocal.ThreadLocalMap inheritableThreadLocals = null;
```

![image-20230318122829897](D:\学习资料\md笔记\imgs\java自身包含的常用类\image-20230318122829897.png)

```java
static ThreadLocalMap createInheritedMap(ThreadLocalMap parentMap) {
    return new ThreadLocalMap(parentMap);
}
```

```java
private ThreadLocalMap(ThreadLocalMap parentMap) {
    Entry[] parentTable = parentMap.table;
    int len = parentTable.length;
    setThreshold(len);
    table = new Entry[len];

    for (int j = 0; j < len; j++) {
        Entry e = parentTable[j];
        if (e != null) {
            @SuppressWarnings("unchecked")
            ThreadLocal<Object> key = (ThreadLocal<Object>) e.get();
            if (key != null) {
                Object value = key.childValue(e.value);
                Entry c = new Entry(key, value);
                int h = key.threadLocalHashCode & (len - 1);
                while (table[h] != null)
                    h = nextIndex(h, len);
                table[h] = c;
                size++;
            }
        }
    }
}
```

inheritableThreadLocal缺点：Thread 的 init 方法中把父Thread的inheritableThreadLocals 变量 copy 了一份给自己。
同样借助 ThreadLocalMap 子线程可以获取到父线程的所有变量。缺点就是 Thread的 init 方法是在线程构造方法中 copy的，
也就是在线程复用的线程池中是没有办法使用的。

如下：

```java
private ExecutorService executorService = new ThreadPoolExecutor(1, 1,
        0L, TimeUnit.MILLISECONDS,
        new LinkedBlockingQueue<>(1024), new CustomizableThreadFactory("demo-pool-"),
        new ThreadPoolExecutor.AbortPolicy());

/**
 * inheritableThreadLocal缺点：Thread 的 init 方法中把父Thread的inheritableThreadLocals 变量 copy 了一份给自己。
 * 同样借助 ThreadLocalMap 子线程可以获取到父线程的所有变量。缺点就是 Thread的 init 方法是在线程构造方法中 copy的，
 * 也就是在线程复用的线程池中是没有办法使用的。
 */
@Test
public void inheritableThreadLocalTest(){
    inheritableThreadLocal.set(9999);
    executorService.submit(() -> {
        System.out.println(Thread.currentThread().getName() + "--->" + inheritableThreadLocal.get());
    });
    inheritableThreadLocal.set(8888);
    executorService.submit(() -> {
        System.out.println(Thread.currentThread().getName() + "--->" + inheritableThreadLocal.get());
    });
    executorService.shutdown();
}
```

输出：

demo-pool-1--->9999
demo-pool-1--->9999

##### TransmittableThreadLocal：

```java
/**
 * TransmittableThreadLocal解决线程复用过程中的ThreadLocal问题
 */
private static ThreadLocal<Integer> transmittableThreadLocal = new TransmittableThreadLocal<>();
private ExecutorService ttlExecutorService = TtlExecutors.getTtlExecutorService(executorService);

@Test
public void transmittableTreadLocalTest(){
    transmittableThreadLocal.set(9999);
    ttlExecutorService.submit(() -> {
        System.out.println(Thread.currentThread().getName() + "--->" + transmittableThreadLocal.get());
    });
    transmittableThreadLocal.set(8888);
    ttlExecutorService.submit(() -> {
        System.out.println(Thread.currentThread().getName() + "--->" + transmittableThreadLocal.get());
    });
    ttlExecutorService.shutdown();
}
```

输出：

demo-pool-1--->9999
demo-pool-1--->8888

#### 三、ConcurrentHashMap

**回顾HashMap:**

HashMap由数组（键值对entry组成的数组主干）+ 链表（元素太多时为解决哈希冲突数组的一个元素上多个entry组成的链表）+ 红黑树（当链表的元素个数达到8链表存储改为红黑树存储）进行数据的存储。默认初始化容量16，加载因子0.75，扩容为原来的2倍。

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\7c57d4b17afb594608ea1ec3233575d0.png)

HashMap采用table数组存储Key-Value的，每一个键值对组成了一个Node节点（JDK1.7为Entry实体，因为jdk1.8加入了红黑树，所以改为Node）。Node节点实际上是一个单向的链表结构，它具有Next指针，可以连接下一个Node节点，以此来解决Hash冲突的问题。

<u>put(key,value)过程：</u>

1.先调用k的hashCode()方法得出哈希值，并通过哈希算法转换成数组的下标。

2.通过上一步哈希算法转换成数组的下标之后，在通过数组下标快速定位到某个位置上。如果这个位置上什么都没有，则返回null。如果这个位置上有单向链表，那么它就会拿着K和单向链表上的每一个节点的K进行equals，如果所有equals方法都返回false，则get方法返回null。如果其中一个节点的K和参数K进行equals返回true，那么此时该节点的value就是我们要找的value了，get方法最终返回这个要找的value。

ConcurrentHashMap在put时加了同步机制(分段锁)保证线程安全，写同步，读无锁

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\f7366e8190fa116d7163b6cdcb835178.png)

**ConcurrentHashMap和HashMap以及Hashtable的区别**：

`HashMap`是线程不安全的，因为`HashMap`中操作都没有加锁，因此在多线程环境下会导致数据覆盖之类的问题，所以，在多线程中使用`HashMap`是会抛出异常的。

`HashTable`是线程安全的,但是`HashTable`只是单纯的在`put()`方法上加上`synchronized`。保证插入时阻塞其他线程的插入操作。虽然安全，但因为设计简单，所以性能低下。

`ConcurrentHashMap`是线程安全的，`ConcurrentHashMap`并非锁住整个方法，而是通过原子操作和局部加锁的方法保证了多线程的线程安全，且尽可能减少了性能损耗。

ConcurrentHashMap的数据结构与HashMap基本类似，区别在于：
1、ConcurrentHashMap在put时加了同步机制(分段锁)保证线程安全，写同步，读无锁；
2、扩容时老数据的转移是并发执行的，这样扩容的效率更高。
3、ConcurrentHashMap的K、V不能为空，HashMap可以。
4、JDK1.8的ConcurrentHashMap采用尾插法

**HashMap线程不安全原因：**

jdk7：transfer()方法造成的死链问题。

jdk8：

```java
if ((p = tab[i = (n - 1) & hash]) == null)
    tab[i] = newNode(hash, key, value, null);
```

HashMap添加元素时，如果是多线程，key的hash值相同且不存在链表时，可能会出现多个线程同时判断(p = tab[i = (n - 1) & hash]) == null判断成立，并都会创建一个新的tab值，添加到Map中，会造成数据覆盖和混乱。

**ConcurrentHashMap使用场景：**

ConcurrentHashMap通常只被看做并发效率更高的Map ,用来替换其他线程安全的Map容器,比如Hashtable和Collections.synchronizedMap.线程安全的容器.特别是Map ,很多情况下一个业务中涉及容器的操作有多个，即复合操作,而在并发执行时,线程安全的容器只能保证自身的数据不被破坏,和数据在多个线程间是可见的,但无法保证业务的行为是否正确。

<u>ConcurrentHashMap的put(key,value)过程：</u>

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\16833947c97dab31b0523f3f7bdf11a6.png)

JDK1.8中 ConcurrentHashMap基于分段锁+CAS保证线程安全，分段锁基于synchronized关键字实现：

![请添加图片描述](D:\学习资料\md笔记\imgs\java自身包含的常用类\062a0395b98a4cedaf92cab14b841be7.png)

#### 四、Atomic

AtomicInteger能够实现整型数据的[原子操作](https://so.csdn.net/so/search?q=原子操作&spm=1001.2101.3001.7020)，在多线程并发的环境下能保证数据安全，而且内部使用乐观锁实现，比使用锁机制的并发性能高。

原子性概念：原子性是指不可分隔，完整性，即某个线程正在做某个业务时，中间不能被分割。要么同时成功，要么同时失败。

```java
volatile int testAtomicityNum1 = 0;

volatile AtomicInteger testAtomicityNum2 = new AtomicInteger(0);

/**
 * volatile不保证原子性  前提：n++在编译后会被拆成三个指令执行：1.拿到原始n 2.进行加1操作 3，把累加后的值进行返回
 * 保证原子性使用Atomic包
 */
@Test
public void testAtomicity() {
    int threadNum = 5;
    int count = 100;
    for (int i = 0; i < threadNum; i++) {
        new Thread(() -> {
            for (int j = 0; j < count; j++) {
                testAtomicityNum1++;
                testAtomicityNum2.getAndIncrement();
            }
        }).start();
    }
    // 程序运行时，模型会有主线程和守护线程。如果超过２个，那就说明上面的２０个线程还有没执行完的，就需要等待
    while (Thread.activeCount()>2){
        Thread.yield();
    }
    System.out.println("testAtomicityNum1累计相加"+threadNum*count+"次，结果为"+testAtomicityNum1);
    System.out.println("testAtomicityNum2累计相加"+threadNum*count+"次，结果为"+testAtomicityNum2);
}
```

输出：

testAtomicityNum1累计相加500次，结果为494
testAtomicityNum2累计相加500次，结果为500

说明Atomic包是可以保证原子性的。

底层说明：

Unsafe 是做一些Java语言不允许但是又十分有用的事情，具体的实现都是native方法，AtomicInteger里调用的 Unsafe 方法 基于的是CPU 的 CAS指令来实现的。所以基于 CAS 的操作可认为是无阻塞的，一个线程的失败或挂起不会引起其它线程也失败或挂起。并且由于 CAS 操作是 CPU 原语，所以性能比较好。

CAS就是Compare and Swap的意思，比较并操作。很多的cpu直接支持CAS指令。CAS是项乐观锁技术，当多个线程尝试使用CAS同时更新同一个变量时，只有其中一个线程能更新变量的值，而其它线程都失败，失败的线程并不会被挂起，而是被告知这次竞争中失败，并可以再次尝试。CAS有3个操作数，内存值V，旧的预期值A，要修改的新值B。当且仅当预期值A和内存值V相同时，将内存值V修改为B，否则什么都不做。 从代码上我们可以看到do while语句，从而证实当更新出现冲突时，即失败时，它还会尝试更新。符合乐观锁的思想。

使用：

使用原子的方式更新基本类型，Atomic包提供了以下3个类。
**AtomicBoolean**：原子更新布尔类型。
**AtomicInteger**：原子更新整型。
**AtomicLong**：原子更新长整型。

通过原子的方式更新数组里的某个元素，Atomic包提供了以3类
**AtomicIntegerArray**：原子更新整型数组里的元素。
**AtomicLongArray**：原子更新长整型数组里的元素。
**AtomicReferenceArray**：原子更新引用类型数组里的元素。

 **AtomicInteger**是一个提供原子操作的**Integer**类，通过线程安全的方式操作加减。

**AtomicInteger**是在使用非阻塞算法实现并发控制，在一些高并发程序中非常适合，但并不能每一种场景都适合，不同场景要使用使用不同的数值类。

这是由硬件提供原子操作指令实现的，这里面用到了一种并发技术：CAS。在非激烈竞争的情况下，开销更小，速度更快。

#### 五、CountDownLatch

CountDownLatch的作用很简单，就是一个或者一组线程在开始执行操作之前，必须要等到其他线程执行完才可以。我们举一个例子来说明，在考试的时候，老师必须要等到所有人交了试卷才可以走。此时老师就相当于等待线程，而学生就好比是执行的线程。

注意：java中还有一个同步工具类叫做CyclicBarrier，他的作用和CountDownLatch类似。同样是等待其他线程都完成了，才可以进行下一步操作，我们再举一个例子，在打王者的时候，在开局前所有人都必须要加载到100%才可以进入。否则所有玩家都相互等待。

我们看一下区别：

CountDownLatch: 一个线程(或者多个)， 等待另外N个线程完成某个事情之后才能执行。 CyclicBarrier : N个线程相互等待，任何一个线程完成之前，所有的线程都必须等待。关键点其实就在于那N个线程（1）CountDownLatch里面N个线程就是学生，学生做完了试卷就可以走了，不用等待其他的学生是否完成（2）CyclicBarrier 里面N个线程就是所有的游戏玩家，一个游戏玩家加载到100%还不可以，必须要等到其他的游戏玩家都加载到100%才可以开局


```java
CountDownLatch countDownLatch = new CountDownLatch(10);

@Test
public void testCountDownLatch() throws InterruptedException {
    long count = countDownLatch.getCount();
    for (int i = 1; i <= count; i++) {
        int finalI = i;
        new Thread(()->{
            //模拟延迟交卷
            if(finalI == 8) {
                try {
                    Thread.sleep(3*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("第"+ finalI +"个学生交卷");
            countDownLatch.countDown();
        }).start();
    }
    countDownLatch.await();
    System.out.println("全部学生都已交卷");
}
```

输出：

第1个学生交卷
第2个学生交卷
第4个学生交卷
第3个学生交卷
第5个学生交卷
第6个学生交卷
第7个学生交卷
第9个学生交卷
第10个学生交卷
第8个学生交卷
全部学生都已交卷

底层：

CountDownLatch有一个内部类Sync，Sync又继承自java.util.concurrent.locks.AbstractQueuedSynchronizer
AbstractQueuedSynchronizer（AQS）是JUC包下的一个极其重要的类，JUC包下的很多实现类都继承自他。当多个线程同时获取同一个锁的时候，没有获取到锁的线程需要排队等待，等锁被释放的时候，队列中的某个线程被唤醒，然后获取锁。AQS中就维护了这样一个同步队列（CLH队列）。

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\41443ba5ebbe4808b0181348f3195b52.png)

AQS中重要的几个属性：

private transient volatile Node head;   //队列的头节点的指针。
private transient volatile Node tail;    //队列的尾节点的指针。
private volatile int state;   //同步器的状态
head指向的是同步队列的头节点，头节点是持有锁资源的节点，tail指向了队列的尾节点。state是什么呢？可以将state理解为资源的状态。

CountDownLatch通过AQS里面的共享锁来实现的，在创建CountDownLatch时候，会传递一个参数count，该参数是锁计数器的初始状态，表示该共享锁能够被count个线程同时获取。当某个线程调用CountDownLatch对象的await方法时候，该线程会等待共享锁可获取时，才能获取共享锁继续运行，而共享锁可获取的的条件是state == 0，而锁倒数计数器的初始值为count，每当一个线程调用该CountDownLatch对象的countDown()方法时候，计数器才-1，所以必须有count个线程调用该countDown()方法后，锁计数器才为0，这个时候等待的线程才能继续运行。

#### 六、ReentrantLock

公平锁：

```java
//设为true为公平锁，false为非公平锁，默认非公平锁
static Lock lock = new ReentrantLock(true);
private static int i = 0;
```

```java
public void testLock() throws InterruptedException {
        CountDownLatch  countDownLatch = new CountDownLatch(2);
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int k = 0; k < 100; k++) {
                    System.out.println(k+"枷锁");
                    lock.lock();
                    try {
                        i++;
                    } finally {
                        lock.unlock();
                    }
                }
                countDownLatch.countDown();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int k = 100; k < 200; k++) {
                    System.out.println(k+"枷锁");
                    lock.lock();
                    try {
                        i++;
                    } finally {
                        lock.unlock();
                    }
                }
                countDownLatch.countDown();
            }
        }).start();
        executorService.shutdown();
        countDownLatch.await();
        if (executorService.isTerminated()){
            System.out.println(i);
        }
    }
```

公平锁执行流程：

![img](D:\学习资料\md笔记\imgs\java自身包含的常用类\0dfd4e89092a4099b8e146eeb59a8a04.png)

与公平锁的lock方法不同，非公平锁在调用lock()方法时，一开始就直接调用CAS方法去获取锁，而不是老老实实地去排队。考虑这样一种情况：锁的持有者(头节点)释放锁时，已经将锁的状态量state设为0，但还没来得及唤醒他的后继节点，这时突然来了一个新的的线程，调用了lock()方法，那么这个新插进来的线程是有可能不排队而直接获取锁的。这就是非公平锁的特点，他并不保证先到先得，排在队列前面的节点是有可能被后来的节点插队的，因此是非公平的

`Condition`，是条件的意思，可以看做是`ReentrantLock`的一种扩展，利用好`Condition`对象，我们可以让线程在合适的时间进行等待，或者在某一个特定的时刻得到通知，取消等待，继续执行。
`Condition`中的主要方法有：

- `await()` 使当前线程等待，同时释放当前锁。当其他线程中使用`signal()`或`signalAll()`方法时，线程会重新获得锁并继续执行
- `awaitUninterruptibly()` 与`await()`方法相同，但不会在等待过程中响应中断
- `singal()` 用于唤醒一个在等待中的线程

例如警匪片中，警察前往歹徒毒品交易的地点，卧底发来消息：交易时间变更，请等待我的通知（调用了`await()`方法）。警察原地待命，30分钟后卧底发来通知：出发（调用了`singal()`方法），警察继续行动。
`Condition`和`ReentrantLock`是配合使用的：


```java
/**
 * Condition，是条件的意思，可以看做是ReentrantLock的一种扩展，利用好Condition对象，我们可以让线程在合适的时间进行等待，
 * 或者在某一个特定的时刻得到通知，取消等待，继续执行
 */
static Condition condition = lock.newCondition();
@Test
public void testCondition() throws InterruptedException {
    CountDownLatch  countDownLatch = new CountDownLatch(1);
    new Thread(new Runnable() {
        @SneakyThrows
        @Override
        public void run() {
            try {
                System.out.println("枷锁");
                lock.lock();
                condition.await();
                System.out.println("going on");
            } finally {
                countDownLatch.countDown();
                lock.unlock();
            }
        }
    }).start();
    Thread.sleep(2000);
    lock.lock();
    condition.signal();
    lock.unlock();
    countDownLatch.await();
}
```

CopyOnWriteArrayList