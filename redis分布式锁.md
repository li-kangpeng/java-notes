###  Redis分布式锁

##### 概念：

分布式锁是控制分布式系统或不同系统之间共同访问共享资源的一种锁实现，如果不同的系统或同一个系统的不同主机之间共享了某个资源时，往往需要互斥来防止彼此干扰来保证一致性。

##### 分布式锁需要具备哪些条件：

1. 互斥性：在任意一个时刻，只有一个客户端持有锁。
2. 无死锁：即便持有锁的客户端崩溃或者其他意外事件，锁仍然可以被获取。
3. 容错：只要大部分Redis节点都活着，客户端就可以获取和释放锁

##### 分布式锁的实现有哪些：

1. 数据库
2. Memcached（add命令）
3. Redis（setnx命令）
4. Zookeeper（临时节点）
5. 等等

##### 单机Redis的分布式锁：

###### 最基础的版本1：

先来一个最基础的版本，代码如下：

```java
public class LockCase1 extends RedisLock {

    public LockCase1(Jedis jedis, String name) {
        super(jedis, name);
    }

    @Override
    public void lock() {
        while(true){
            String result = jedis.set(lockKey, "value", NOT_EXIST);
            if(OK.equals(result)){
                System.out.println(Thread.currentThread().getId()+"加锁成功!");
                break;
            }
        }
    }

    @Override
    public void unlock() {
        jedis.del(lockKey);
    }
}
```

LockCase1类提供了lock和unlock方法。 其中lock方法也就是在reids客户端执行如下命令：

```
SET lockKey value NX
```

而unlock方法就是调用DEL命令将键删除。 好了，方法介绍完了。现在来想想这其中会有什么问题？ 假设有两个客户端A和B，A获取到分布式的锁。A执行了一会，突然A所在的服务器断电了（或者其他什么的），也就是客户端A挂了。这时出现一个问题，这个锁一直存在，且不会被释放，其他客户端永远获取不到锁。如下示意图

![image-20230531151056980](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20230531151056980.png)

可以通过设置过期时间来解决这个问题

###### 版本2-设置锁的过期时间：

```java
public void lock() {
    while(true){
        String result = jedis.set(lockKey, "value", NOT_EXIST,SECONDS,30);
        if(OK.equals(result)){
            System.out.println(Thread.currentThread().getId()+"加锁成功!");
            break;
        }
    }
}
```

类似的Redis命令如下

```
SET lockKey value NX EX 30
```

注：要保证设置过期时间和设置锁具有原子性

这时又出现一个问题，问题出现的步骤如下

1. 客户端A获取锁成功，过期时间30秒。
2. 客户端A在某个操作上阻塞了50秒。
3. 30秒时间到了，锁自动释放了。
4. 客户端B获取到了对应同一个资源的锁。
5. 客户端A从阻塞中恢复过来，释放掉了客户端B持有的锁。

示意图如下

![image-20230531151038684](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20230531151038684.png)

这时会有两个问题

1. 过期时间如何保证大于业务执行时间?
2. 如何保证锁不会被误删除?

先来解决如何保证锁不会被误删除这个问题。 这个问题可以通过设置value为当前客户端生成的一个随机字符串，且保证在足够长的一段时间内在所有客户端的所有获取锁的请求中都是唯一的。

###### 版本3-设置锁的value

抽象类RedisLock增加lockValue字段，lockValue字段的默认值为UUID随机值假设当前线程ID。

```java
public abstract class RedisLock implements Lock {

    //...
    protected String lockValue;

    public RedisLock(Jedis jedis,String lockKey) {
        this(jedis, lockKey, UUID.randomUUID().toString()+Thread.currentThread().getId());
    }

    public RedisLock(Jedis jedis, String lockKey, String lockValue) {
        this.jedis = jedis;
        this.lockKey = lockKey;
        this.lockValue = lockValue;
    }

    //...
}
```

加锁代码

```java
public void lock() {
    while(true){
        String result = jedis.set(lockKey, lockValue, NOT_EXIST,SECONDS,30);
        if(OK.equals(result)){
            System.out.println(Thread.currentThread().getId()+"加锁成功!");
            break;
        }
    }
}
```

解锁代码

```java
public void unlock() {
    String lockValue = jedis.get(lockKey);
    if (lockValue.equals(lockValue)){
        jedis.del(lockKey);
    }
}
```

i++问题：

i++操作也可分为三个步骤：读i的值，进行i+1，设置i的值。 如果两个线程同时对i进行i++操作，会出现如下情况

1. i设置值为0
2. 线程A读到i的值为0
3. 线程B也读到i的值为0
4. 线程A执行了+1操作，将结果值1写入到内存
5. 线程B执行了+1操作，将结果值1写入到内存
6. 此时i进行了两次i++操作，但是结果却为1

在多线程环境下有什么方式可以避免这类情况发生? 解决方式有很多种，例如用AtomicInteger、CAS、synchronized等等。 这些解决方式的目的都是要确保i++ 操作的原子性。那么回过头来看看解锁，同理我们也是要确保解锁的原子性。我们可以利用Redis的lua脚本来实现解锁操作的原子性。

###### 版本4-具有原子性的释放锁

lua脚本内容如下

```javascript
if redis.call("get",KEYS[1]) == ARGV[1] then
    return redis.call("del",KEYS[1])
else
    return 0
end
```

这段Lua脚本在执行的时候要把的lockValue作为ARGV[1]的值传进去，把lockKey作为KEYS[1]的值传进去。现在来看看解锁的java代码:

```java
public void unlock() {
    // 使用lua脚本进行原子删除操作
    String checkAndDelScript = "if redis.call('get', KEYS[1]) == ARGV[1] then " +
                                "return redis.call('del', KEYS[1]) " +
                                "else " +
                                "return 0 " +
                                "end";
    jedis.eval(checkAndDelScript, 1, lockKey, lockValue);
}
```

###### 版本5-确保过期时间大于业务执行时间

抽象类RedisLock增加一个boolean类型的属性isOpenExpirationRenewal，用来标识是否开启定时刷新过期时间。 在增加一个scheduleExpirationRenewal方法用于开启刷新过期时间的线程。

```java
public abstract class RedisLock implements Lock {
	//...

    protected volatile boolean isOpenExpirationRenewal = true;

    /**
     * 开启定时刷新
     */
    protected void scheduleExpirationRenewal(){
        Thread renewalThread = new Thread(new ExpirationRenewal());
        renewalThread.start();
    }

    /**
     * 刷新key的过期时间
     */
    private class ExpirationRenewal implements Runnable{
        @Override
        public void run() {
            while (isOpenExpirationRenewal){
                System.out.println("执行延迟失效时间中...");

                String checkAndExpireScript = "if redis.call('get', KEYS[1]) == ARGV[1] then " +
                        "return redis.call('expire',KEYS[1],ARGV[2]) " +
                        "else " +
                        "return 0 end";
                jedis.eval(checkAndExpireScript, 1, lockKey, lockValue, "30");

                //休眠10秒
                sleepBySencond(10);
            }
        }
    }
}
```

加锁代码在获取锁成功后将isOpenExpirationRenewal置为true，并且调用scheduleExpirationRenewal方法，开启刷新过期时间的线程。

```java
public void lock() {
    while (true) {
        String result = jedis.set(lockKey, lockValue, NOT_EXIST, SECONDS, 30);
        if (OK.equals(result)) {
            System.out.println("线程id:"+Thread.currentThread().getId() + "加锁成功!时间:"+LocalTime.now());

            //开启定时刷新过期时间
            isOpenExpirationRenewal = true;
            scheduleExpirationRenewal();
            break;
        }
        System.out.println("线程id:"+Thread.currentThread().getId() + "获取锁失败，休眠10秒!时间:"+LocalTime.now());
        //休眠10秒
        sleepBySencond(10);
    }
}
```

解锁代码增加一行代码，将isOpenExpirationRenewal属性置为false，停止刷新过期时间的线程轮询。

```java
public void unlock() {
    //...
    isOpenExpirationRenewal = false;
}
```

###### 测试:

测试代码如下

```java
public void testLockCase5() {
    //定义线程池
    ThreadPoolExecutor pool = new ThreadPoolExecutor(0, 10,
                                                    1, TimeUnit.SECONDS,
                                                    new SynchronousQueue<>());

    //添加10个线程获取锁
    for (int i = 0; i < 10; i++) {
        pool.submit(() -> {
            try {
                Jedis jedis = new Jedis("localhost");
                LockCase5 lock = new LockCase5(jedis, lockName);
                lock.lock();

                //模拟业务执行15秒
                lock.sleepBySencond(15);

                lock.unlock();
            } catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    //当线程池中的线程数为0时，退出
    while (pool.getPoolSize() != 0) {}
}
```

或许到这里基于单机Redis环境的分布式就介绍完了。但是使用java的同学有没有发现一个锁的重要特性.那就是锁的重入，那么分布式锁的重入该如何实现呢？这里就留一个坑了

#### Redission:

```xml
<dependency>
    <groupId>org.redisson</groupId>
    <artifactId>redisson-spring-boot-starter</artifactId>
    <version>3.17.6</version>
</dependency>
@Configuration
public class MyRedissonConfig {

    /**
     * 所有对Redisson的使用都是通过RedissonClient
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod="shutdown")
    public RedissonClient redissonClient() throws IOException {
        //1、创建配置
        Config config = new Config();
        // 连接必须要以 redis 开头~ 有密码填密码
        config.useSingleServer().setAddress("redis://IP地址:6379").setPassword("000415");
        //2、根据Config创建出RedissonClient实例
        //Redis url should start with redis:// or rediss://
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }
}
/**
 * @description:
 * @author: Ning Zaichun
 * @date: 2022年09月20日 20:59
 */
@Service
public class RedissonServiceImpl implements IRedissonService {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MenuMapper menuMapper;

    private static final String REDISSON_MENU_LIST = "redisson:menu:list";
    private static final String REDISSON_MENU_LIST_LOCK_VALUE = "redisson:lock";
    @Autowired
    private RedissonClient redissonClient;


    @Override
    public List<MenuEntity> getList() {
        // 判断缓存是否有数据
        String menuJson = stringRedisTemplate.opsForValue().get(REDISSON_MENU_LIST);
        if (menuJson != null) {
\

            System.out.println("缓存中有，直接返回缓存中的数据");
            List<MenuEntity> menuEntityList = JSON.parseObject(menuJson, new TypeReference<List<MenuEntity>>() {
            });
            return menuEntityList;
        }
        // 从数据库中查询
        List<MenuEntity> result = getMenuJsonFromDbWithRedissonLock();
        return result;
    }


    /**
     * 问题：其实写成上面那种模样，相对来说，也能解决很多时候的问题了，从头到尾看过来的话，其实也能发现就一个锁自动过期问题没有解决了。
     * 但在实现这个之前，我还是说明一下，为什么说在没有解决锁自动过期问题时，就已经能应付大多数场景了。
     * 重点在于如何评估 锁自动过期时间，锁自动过期时间到底设置多少合适呢？
     * 其实如果对于业务理解较为透彻，对于这一部分的业务代码执行时间能有一个较清晰的估算，给定一个合适的时间，在不出现极端情况，基本都能应付过来了。
     * <p>
     * 但是呢，很多时候，还是会怕这个万一的，万一真出现了，可能造成的损失就不止 一万了，哈哈。
     * 解决方案
     * 1、在市场主流的 Redission 中，针对这样的问题，已经有了解决方案。这也是Redission中常说的看门狗机制。
     * <p>
     * 如果需要自己实现的思路：
     * 1、这方面的问题，也做了十分浅显的思考，我觉得应该还是依赖于定时任务去实现，但到底该如何实现这个定时任务，我还没法给出一个合适的解决方案。 或许我应该会尝试一下。
     *
     * @return
     */
    public List<MenuEntity> getMenuJsonFromDbWithRedissonLock() {
        System.out.println("从数据库中查询");
        //1、占分布式锁。去redis占坑
        //（锁的粒度，越细越快:具体缓存的是某个数据，11号商品） product-11-lock
        //RLock catalogJsonLock = redissonClient.getLock("catalogJson-lock");
        //创建读锁
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(REDISSON_MENU_LIST_LOCK_VALUE);
        RLock rLock = readWriteLock.readLock();
        List<MenuEntity> result = null;
        try {
            rLock.lock();
            String menuJson = stringRedisTemplate.opsForValue().get(REDISSON_MENU_LIST);
            if (menuJson != null) {
                System.out.println("缓存中有，直接返回缓存中的数据");
                List<MenuEntity> menuEntityList = JSON.parseObject(menuJson, new TypeReference<List<MenuEntity>>() {
                });
                return menuEntityList;
            }
            try {
                Thread.sleep(50000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            //加锁成功...执行业务
            //加锁成功...执行业务
            result = menuMapper.selectList(new QueryWrapper<MenuEntity>());
            // 构建缓存
            stringRedisTemplate.opsForValue().set(REDISSON_MENU_LIST, JSON.toJSONString(result));
        } finally {
            rLock.unlock();
        }
        return result;
    }


    @Override
    public Boolean updateMenuById(MenuEntity menu) {
        return updateMenuWithRedissonLock(menu);
    }

    public Boolean updateMenuWithRedissonLock(MenuEntity menu) {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(REDISSON_MENU_LIST_LOCK_VALUE);
        RLock writeLock = readWriteLock.writeLock();
        Boolean update = false;
        try {
            writeLock.lock();
            //加锁成功...执行业务
            //加锁成功...执行业务
            update = menuMapper.updateById(menu) > 0;
        } finally {
            writeLock.unlock();
        }
        return update;
    }

}
```

Redisson 默认锁过期时间 30s，一旦进行修改了的话，Redisson会取消此锁的自动续期机制。 这一步的源码在 RedissonLock的tryAcquireAsync中

![image-20230531151014103](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20230531151014103.png)