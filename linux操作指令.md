## linux操作指令

根据端口号进行服务的关闭：

步骤一：确定8080的进程号

[root@localhost ~]# netstat -antup|grep 8080

![img](D:\学习资料\md笔记\imgs\linux操作指令\1751005-20200304212128398-674107549.png)

步骤二：通过进程号确定服务目录

[root@localhost ~]# ll /proc/32507/cwd   #此处可以分步骤执行，我这里直接整合成一个命令了
[root@localhost ~]# pwdx  32507   #此处根据网友评论区提供，效果同上一个命令，更加快捷

![img](D:\学习资料\md笔记\imgs\linux操作指令\1751005-20200304212249503-429392497.png)