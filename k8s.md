1.关闭防火墙

sudo ufw disable

2.关闭sawp

swapoff -a

sed -i '/ swap / s/^/#/' /etc/fstab

3.安装docker

##### step 1: 安装必要的一些系统工具

更新索引包

sudo apt-get update

获取HTTPS支持

$sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common

##### step 2: 安装GPG证书

$curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -

##### step 3: 写入软件源信息

$ sudo add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"

##### Step 4: 更新并安装 Docker-CE

$sudo apt-get -y update

列出可用的 Docker 版本

apt-cache madison docker-ce

从列出的版本中，根据需要选择要安装的特定版本号（注意docker版本号和k8s要兼容）

sudo apt-get install docker-ce=5:20.10.24~3-0~ubuntu-focal containerd.io

查看docker状态并启动

systemctl start docker

systemctl status docker



k8s

##### 添加 K8s 安装密钥

sudo apt update &&  sudo apt install -y apt-transport-https curl

curl -s https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg |  sudo apt-key add -

##### 配置 K8S 源

sudo  echo  "deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main"  >> /etc/apt/sources.list.d/kubernetes.list

保证 /etc/apt/sources.list.d/kubernetes.list文件含有deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main

![](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231212144858968.png)

##### 更新apt 包, 安装kubelet, kubeadm and kubectl

将上述的kubernetes.list源加载

sudo apt-get update

安装指定版本

sudo apt-get install -y kubelet=1.22.2-00 kubeadm=1.22.2-00 kubectl=1.22.2-00

保持版本取消自动更新

sudo apt-mark hold kubelet kubeadm kubectl

备注：可通过apt-cache show kubectl查看包有哪些版本可以供安装，以便于指定安装版本。



kubeadm init --image-repository registry.aliyuncs.com/google_containers --kubernetes-version v1.22.2 --pod-network-cidr=39.104.0.0/16 --apiserver-advertise-address=39.104.132.115