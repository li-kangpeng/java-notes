CPU飙高

1.使用docker stats

![image-20240524162501938](https://gitee.com/li-kangpeng/images/raw/master/image-20240524162501938.png)

2.进入对应容器内部

```
docker exec -it wxdevops-service /bin/sh
```

3.使用top命令查看哪个进程cpu最高

```
top
```

![image-20240524163206650](https://gitee.com/li-kangpeng/images/raw/master/image-20240524163206650.png)

4.查看进程下的哪个线程cpu飙高（7为进程pid）

```
top -Hp 7
```

![image-20240524163234603](https://gitee.com/li-kangpeng/images/raw/master/image-20240524163234603.png)

5.将线程号转为16进制（51为线程pid），我的结果为33

```
printf "%x\n" 51
```

6.将进程信息打印出来 （7为进程pid）

```
jstack 7  > thread_dump.txt
```

7.退出容器，将容器内的进程信息copy到外面

```
docker cp wxdevops-service:thread_dump.txt thread_dump.txt
```

8.txt查找出错信息

直接文件内搜索’0x33‘,这里的33是我的线程十六进制pid

结果如下

![image-20240524163300676](https://gitee.com/li-kangpeng/images/raw/master/image-20240524163300676.png)

对应代码块

![image-20240524163249183](https://gitee.com/li-kangpeng/images/raw/master/image-20240524163249183.png)

###### 可能遇到的问题及解决：

1.此使用docker进行java项目的打包为镜像部署的，镜像文件可参考如下

```dockerfile
# 基础镜像  更改为使用JDK镜像，这里以Alpine为例，体积更小。若需Debian可继续使用Debian的JDK镜像
# 本来使用的openjdk:8-jre，但是由于不是完整的jdk不能使用jstack 命令所以使用完整的jdk
FROM openjdk:8-jdk-alpine

# author
LABEL MAINTAINER="zhsd"

# 更新软件包列表并安装常用工具（如果是Debian镜像，使用apt-get）Alpine Linux的包管理命令是apk
# 这里确定进入容器后可使用top命令
RUN apk add --no-cache procps nano vim less  

# 挂载目录、创建目录、复制jar、设置时区和启动命令等保持不变
VOLUME /home/wx
RUN mkdir -p /home/wx
COPY ./wcyy-service-0.0.1-SNAPSHOT.jar /home/wx/wxdevops-service.jar
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENTRYPOINT ["java","-Duser.timezone=Asia/Shanghai","-jar","-XX:+HeapDumpOnOutOfMemoryError","-XX:HeapDumpPath=/home/app/dumps/heapdump.hprof","/home/wx/wxdevops-service.jar"]
```

2.在docker容器启动的时候务必加上--init，如

```
docker run -t --init  -d --restart=always --name wxdevops-service  -p 10003:10003 wxdevops-service;
```

否则可能报错jstack Unable to get pid of LinuxThreads manager thread





OOM排查:

不推荐：

通过 jmap 指定打印他的内存快照 dump。（Dump 文件是进程的内存镜像。可以把程序的执行状态通过调试器保存到 dump 文件中）

```
jmap -dump:format=b,file=heap.hprof pid
```

推荐：

1.项目启动时加入参数-XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=/home/app/dumps/heapdump.hprof

 指定当 OOM 发生时自动 dump 堆内存信息到指定目录

2.此时文件在docker内部，copy出来

3.windows上安装VisualVM

通过工具，VisualVM（Ecplise MAT）去分析 dump 文件。VisualVM 可以加载离线的 dump 文件，文件 -> 装入 -> 选择 dump 文件即可查看堆快照信息。如下图

![image-20240529153848332](https://gitee.com/li-kangpeng/images/raw/master/image-20240529153848332.png)