## Nginx

#### 反向代理：

概念：[反向代理](https://so.csdn.net/so/search?q=反向代理&spm=1001.2101.3001.7020)（Reverse Proxy）方式是指以代理服务器来接受Internet上的连接请求，然后将请求转发给内部网络上的服务器，并将从服务器上得到的结果返回给Internet上请求连接的客户端，此时代理服务器对外就表现为一个服务器。

有反向代理，当然也存在正向代理的概念咯。正向代理指的是，一个位于客户端和原始服务器之间的服务器，为了从原始服务器取得内容，(原始服务器)，然后代理向原始服务器转交请求并将获得的内容返回给客户端。

***正向代理代理对象是客户端，反向代理代理对象是服务端。***

##### 优点：

- 可以起到保护网站安全的作用，因为任何来自Internet的请求都必须先经过代理服务器。
- 通过缓存静态资源，加速Web请求。
- 实现负载均衡。顺便说下，目前市面上，主流的负载均衡方案，硬件设备有F5，软件方案有四层负载均衡的LVS，七层负载均衡的Nginx、Haproxy等。
- Nginx 特点是占有内存少，并发处理能力强，以高性能、低系统资源消耗而闻名，Nginx官方测试为5万并发请求。

##### Nginx集群默认算法:

 nginx的upstream⽬前⽀持4种⽅式的分配
 1、轮询（默认）
 每个请求按时间顺序逐⼀分配到不同的后端服务器，如果后端服务器down掉，能⾃动剔除。

**** 这里的轮询并不是每个请求轮流分配到不同的后端服务器，与ip_hash类似，但是按照访问url的hash结果来分配请求，使得每个url定向到同一个后端服务器，主要应用于后端服务器为缓存时的场景下****

upstream backserver { 

​       server 127.0.0.1:8080; 

​       server 127.0.0.1:9090; 

} 

 2、weight
 指定轮询⼏率，weight和访问⽐率成正⽐，⽤于后端服务器性能不均的情况。

****每个请求按一定比例分发到不同的后端服务器，weight值越大访问的比例越大，用于后端服务器性能不均的情况****

upstream backserver { 

​       server 192.168.0.14 weight=5; 

​       server 192.168.0.15 weight=2; 

} 

 3、ip_hash
 每个请求按访问ip的hash结果分配，这样每个访客固定访问⼀个后端服务器，可以解决session的问题。

****ip_hash也叫IP绑定，每个请求按访问ip的hash值分配，这样每个访问客户端会固定访问一个后端服务器，可以解决会话Session丢失的问题****

算法：hash("124.207.55.82") % 2 = 0, 1

​     upstream backserver { 

​       ip_hash; 

​       server 127.0.0.1:8080; 

​       server 127.0.0.1:9090; 

}

 4、最少连接

****web请求会被转发到连接数最少的服务器上****

​     upstream backserver { 

​       least_conn;

​       server 127.0.0.1:8080; 

​       server 127.0.0.1:9090; 

}    

 5、fair（第三⽅）
 按后端服务器的响应时间来分配请求，响应时间短的优先分配。
 6、url_hash（第三⽅）
 按访问url的hash结果来分配请求，使每个url定向到同⼀个后端服务器，后端服务器为缓存时⽐较有效。

##### 前置操作：

Nginx的安装需要确定Linux安装相关的几个库，否则配置和编译会出现错误， 具体的检查安装过程为：

（1）  gcc编译器是否安装

​     检查是否安装：yum list installed | grep gcc

​     执行安装：yum install gcc -y

（2）  openssl库是否安装

​     检查是否安装：yum list installed | grep openssl

​     执行安装：yum install openssl openssl-devel -y

（3）  pcre库是否安装

​     检查是否安装：yum list installed | grep pcre

​     执行安装：yum install pcre pcre-devel -y

（4）  zlib库是否安装

​     检查是否安装：yum list installed | grep zlib

​     执行安装：yum install zlib zlib-devel -y

（5）  一次性安装，执行如下命令

yum install gcc openssl openssl-devel pcre pcre-devel zlib zlib-devel -y

#### 静态部署：

1.

![image-20221001114402451](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001114402451.png)

使用配置文件进行启动：

![image-20221001114820614](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001114820614.png)

![image-20221001114919336](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001114919336.png)

2.修改配置文件

![image-20221001115125278](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001115125278.png)

重启服务：

![image-20221001115224950](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001115224950.png)



![image-20221001115301541](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001115301541.png)

替换：

​          http://192.168.239.130/ = root = /opt/static/ace

​          http://192.168.239.130:80/ace = root/ace = /opt/static/ace/ace  （所以配置location为/ace但root为/opt/static/ace时无法访问）

启动nginx并访问日志：

![image-20221001132234911](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001132234911.png)

#### Nginx对Tomcat负载均衡：

前置操作：

1.复制两份tomcat，删除webapps没用的文件，保留以下：

![image-20221001144739687](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001144739687.png)

2.修改tomcat配置信息：

第一台：

![image-20221001144834088](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001144834088.png)

第二台：

![image-20221001144933808](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001144933808.png)

![image-20221001144923573](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001144923573.png)

![image-20221001144944057](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001144944057.png)

3.将项目war包上传至各个tomcat的webapps目录下，并进行启动，启动时便查看日志命令：

./startup.sh |tail -f ../logs/catalina.out

4.在浏览器使用ip：端口/html的上级文件名进行访问。（两个tomcat都进行访问：tomcat端口不同）

配置Nginx：

http模块加上：

![image-20221001145723377](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001145723377.png)

server模块加上：

![image-20221001145800865](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001145800865.png)

使用浏览器访问  ip+nginx配置的端口号+location的地址即可。

![image-20221001150119294](https://gitee.com/li-kangpeng/java-notes/raw/master/image-20221001150119294.png)

负载均衡策略参考上文。



其他配置：

****配置1：****

upstream backserver { 

​       server 127.0.0.1:9100;

​         \#其它所有的非backup机器down的时候，才请求backup机器

​       server 127.0.0.1:9200 backup; 

} 

****配置2：****

​     upstream backserver { 

server 127.0.0.1:9100;

\#down表示当前的server是down状态，不参与负载均衡

  server 127.0.0.1:9200 down; 

} 

一般在项目上线的时候，可以分配部署不同的服务器上，然后对Nginx重新reload。

reload不会影响用户的访问，或者可以给一个提示页面,系统正在升级...



##### 静态代理实现：

**方式一 在nginx.conf的location中配置静态资源的后缀**

****例如：当访问静态资源，则从linux服务器/opt/static目录下获取（举例）****

location ~ .*\.(js|css|htm|html|gif|jpg|jpeg|png|bmp|swf|ioc|rar|zip|txt|flv|mid

|doc|ppt|pdf|xls|mp3|wma)$ {

​       root /opt/static;

}

**说明**

Ø ~ 表示正则匹配，也就是说后面的内容可以是正则表达式匹配

Ø 第一个点 . 表示任意字符

Ø *表示一个或多个字符

Ø \. 是转移字符，是后面这个点的转移字符

Ø | 表示或者

Ø $ 表示结尾

整个配置表示以 .后面括号里面的这些后缀结尾的文件都由nginx处理

放置静态资源的目录，要注意一下目录权限问题，如果权限不足，给目录赋予权限；

否则会出现403错误 chmod 755     

**方式二 在nginx.conf的location中配置静态资源所在目录实现**

****例如：当访问静态资源，则从linux服务器/opt/static目录下获取（举例）****

location ~ .*/(css|js|img|images) {

​       root  /opt/static;

}

​     xxx/css

xxx/js

xxx/img

xxx/images

​     我们将静态资源放入 /opt/static 目录下，然后用户访问时由nginx返回这些静态资源



在真实项目中，war包下的静态资源文件夹需复制到Nginx的lcation的配置目录下，根据图片访问路径创建文件夹。

例如：

图片访问路径为：http://192.168.239.130/myweb/image/001.jpg

第一步：则需要在Nginx的配置文件配置

location ~ .*\.(js|css|htm|html|gif|jpg|jpeg|png|bmp|swf|ioc|rar|zip|txt|flv|mid

|doc|ppt|pdf|xls|mp3|wma)$ {

​       root /opt/static;   #此处为文件位置，表示以（）中字符串.结尾的去opt/static目录下寻找文件

}



第二步：

在/opt/static目录下创建文件夹/myweb/并将war包下的resource目录下的资源文件夹放入其中。







nginx存放多个前端项目，每个前端都放在html文件夹不不同目录下面

nginx的conf内容如下：

```shell
worker_processes  1;
events {
   #配置每个worker进程连接数上限，nginx支持的总连接数就等于worker processes * worker connections
    worker_connections  1024;
}
http{
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;

   server {
       listen       9009;
          server_name  192.168.5.188;
       client_max_body_size 1024M;

       location /earth-service {
         alias /usr/share/nginx/html/3dtiles/earth-service;
         try_files $uri $uri/ alias /usr/share/nginx/html/3dtiles/earth-service/index.html;
       }
       location /maps {
         alias /usr/share/nginx/html/3dtiles/maps;
         try_files $uri $uri/ alias /usr/share/nginx/html/3dtiles/maps/index.html;
      }

      location /intergration-front {
          alias /usr/share/nginx/html/intergration-front;
          try_files $uri $uri/ /usr/share/nginx/html/intergration-front/index.html;
      }

       location / {
           root   html;
              index  index.html index.htm;
      }

       error_page   500 502 503 504  /50x.html;
       location = /50x.html {
           root   html;
       }

   }
}
```

nginx根据uri地址前缀匹配不同的后端项目进行转发请求：

```shell
worker_processes  1;

events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    server {
        listen       80;
        server_name  192.168.5.188;
        client_max_body_size 1024M;

        location /hlw {
            alias /usr/share/nginx/html/hlw;
            try_files $uri $uri/ /usr/share/nginx/html/gaw/index.html;
        }

        location /gaw {
            alias /usr/share/nginx/html/gaw;
            try_files $uri $uri/ /usr/share/nginx/html/gaw/index.html;
        }

         location /gawApi/ {
				# rewrite ^/gawApi(.*)$ $1 break;  # 去掉 /gawApi 部分的 URI
                    proxy_pass http://192.168.5.188:6306/;  # 将请求代理到本地的 8080 端口
                    proxy_set_header Host $host;
                    proxy_set_header X-Real-IP $remote_addr;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                    proxy_set_header X-Forwarded-Proto $scheme;
                }

         location /hlwApi/ {
                            proxy_pass http://192.168.5.188:10005/;  # 将请求代理到本地的 8080 端口
                            proxy_set_header Host $host;
                            proxy_set_header X-Real-IP $remote_addr;
                            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                            proxy_set_header X-Forwarded-Proto $scheme;
                        }

        location / {
            root   html;
            index  index.html index.htm;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
    }
}
```

