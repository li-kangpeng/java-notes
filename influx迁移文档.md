#### influx迁移文档

1. 查询原有镜像搭载目录

docker inspect --format='{{.Mounts}}' 容器名称

2. 挂载迁移

scp 挂载目录 目标服务器用户名@目标服务器ip:目标服务器目录

输入密码  完成挂载目录的拷贝

3. 迁移容器

先把数据库容器保存为镜像：

docker commit 容器ID 新镜像名

把镜像保存为压缩文件

docker save 新镜像ID > /目录/influxdb_20210705.tar

把压缩文件拷贝到新服务器

scp influxdb_20210705.tar root@IP地址:/home

在新服务器上载入镜像文件

docker load -i influxdb_20210705.tar

使用`docker tag`命令为加载的镜像指定名称和标签

docker tag <image-id> <new-image-name>:<tag>

在新服务器上加载数据库容器

docker run -idt --name 容器名称 -p 8086:8086 -v 挂载目录:/var/lib/influxdb 容器名称

国生项目为：

```tex
docker run -itd --name influxdb2 /
 -p 8086:8086 /
 -v /home/zh185/influxdb2/data:/var/lib/influxdb2 /
 -v /home/zh185/influxdb2/conf:/etc/influxdb2 /
 --restart=always
 influxdb2
```



```
docker run -p 9000:9000 -p 9006:9006 \
 --name minio \
 -d --restart=always \
 -v /home/zh185/minio/data:/data \
 -v /home/zh185/minio/conf:/root/.minio \
  minio server \
 /data --console-address ":9006" --address ":9000"
```






