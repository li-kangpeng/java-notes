### docker-compose

前置：熟练掌握并安装docker

1.安装docker-compose

```sh
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

赋予权限：

```sh
sudo chmod +x /usr/local/bin/docker-compose
```

验证docker-compose是否成功安装：

```sh
docker-compose --version
```

2.编写docker-compose.yml脚本

该脚本主要包含容器的启动命令、参数等信息。

示例：

```yml
version: '3'  # 指定Compose文件的版本，可以根据需要更改

services:
  mysql:
    image: mysql:latest
    container_name: compose_mysql  # 指定容器的名称
    ports:
      - "3306:3306"  # 映射端口格式应该是 "宿主机端口:容器端口"
    volumes:
      - /reload_dir/mysql/conf:/etc/mysql/conf.d
      - /reload_dir/mysql/logs:/var/log/mysql
      - /reload_dir/mysql/data:/var/lib/mysql
    environment:
      - "MYSQL_ROOT_PASSWORD=qwer12340@"

  redis:
    image: redis:latest
    container_name: compose_redis  # 指定容器的名称
    ports:
      - "6378:6379"
    volumes:
      - /reload_dir/redis/data:/data
      - /reload_dir/redis/redis.conf:/etc/redis/redis.conf
      - /reload_dir/redis/log/redis.log:/var/log/redis.log
    command: ["redis-server", "--requirepass qwer12340@"]

  nacos:
    image: nacos/nacos-server:latest
    container_name: compose_nacos
    ports:
      - "8848:8848"
    volumes:
      - /reload_dir/nacos/init.d/custom.properties:/home/nacos/init.d/custom.properties
      - /reload_dir/nacos/logs:/home/nacos/logs
    environment:
      - "MODE=standalone"
    cap_add:
      - SYS_ADMIN #添加特权相当于--privileged=true
    # 指定依赖关系，说明只有在mysql启动之后才进行nacos的启动
    depends_on:
      - mysql   #这里填入services的下一级即可

```

3.启动

进入要启动的docker-compose.yml的上一级，输入命令：

```sh
docker-compose -f ./docker-compose.yml -p test up -d
```

-f为file，指定要启动的docker-compose文件，可不填，不填默认文件名为docker-compose.yml。

-p为项目名，可不填。

-d表示后台启动。

启动后如下所示：

![image-20230823173038388](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20230823173038388.png)

4.停止

停止单个容器使用docker stop停止即可。

停止docker-compose使用如下命令：

```sh
docker-compose -f docker-compose.yml down
```

5.如果多个docker-compose有先后启动关系，请使用sh脚本启动。