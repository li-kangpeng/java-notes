配置：

1.下载python版本3.10.0，建议这个，虽然写的>=3.10,但是使用3.12的时候会报错，下载地址https://www.python.org/downloads/windows/

![image-20240606151022684](https://gitee.com/li-kangpeng/images/raw/master/image-20240606151022684.png)

进行安装，安装时记得勾选添加环境变量，当然不嫌麻烦的话可以自己配一下。

2.配置项目使用python版本

![image-20240607091030462](https://gitee.com/li-kangpeng/images/raw/master/image-20240607091030462.png)

3.项目根目录新建models文件，下载text2vec-large-chinese文件到此文件中，下载地址https://huggingface.co/GanymedeNil/text2vec-large-chinese

4.配置env文件内容本地开发环境（通义千问为例）

配置使用代理模式

```env
# 这个如果想配置其他的参考dbgpt/configs/model_config.LLM_MODEL_CONFIG文件中搜索proxyllm即可
# LLM_MODEL写key值
LLM_MODEL=tongyi_proxyllm
# 如果使用openai代理需要配置这个，其他不要配置
# PROXYLLM_BACKEND=chatgpt_proxyllm
```

申请通义千问API-KEY，地址https://dashscope.console.aliyun.com/apiKey，然后配置如下

```
PROXY_API_KEY=你刚申请的API-Key
PROXY_SERVER_URL=https://dashscope.aliyuncs.com/compatible-mode/v1/chat/completions
```

5.安装依赖

```python
pip install "dbgpt[default]" -i https://pypi.tuna.tsinghua.edu.cn/simple
```

此操作使用清华镜像源安装所列依赖，但不会安装ai的依赖，需另行安装。

Tips：如果安装异常  直接重启就完了  解决99的问题

另行安装图数据库neo4j

```python
pip install neo4j
```

6.启动项目，打开dbgpt/app/dbgpt_server.py文件，点击最下面的if那行有个运行图标便可以直接运行了。

到这项目就可以启动看到页面了，默认访问地址http://localhost:5670/

7.docker安装图数据库Tugraph

```sh
 #拉取镜像
 docker pull tugraph/tugraph-runtime-ubuntu18.04
 #启动
 docker run -it -d -p 7070:7070 -p 7687:7687 -p 8000:8000 -p 9090:9090 \
 -v /home/tugraph/data:/var/lib/lgraph/data  -v /home/tugraph/log:/var/log/lgraph_log \
 --name tugraph tugraph/tugraph-runtime-ubuntu18.04:latest /bin/bash
```

修改配置文件图存储使用TuGraph

```
GRAPH_STORE_TYPE=TuGraph
### TuGraph config
TUGRAPH_HOST=192.168.5.185
TUGRAPH_PORT=7687
TUGRAPH_USERNAME=admin
TUGRAPH_PASSWORD=zh@2021
TUGRAPH_VERTEX_TYPE=entity
TUGRAPH_EDGE_TYPE=relation
TUGRAPH_EDGE_NAME_KEY=label
```

8.修改配置文件使用Mysql存储历史对话

```
CHAT_HISTORY_STORE_TYPE=db
LOCAL_DB_TYPE=mysql

### MYSQL database
LOCAL_DB_TYPE=mysql
LOCAL_DB_USER=root
LOCAL_DB_PASSWORD=123456
LOCAL_DB_HOST=127.0.0.1
LOCAL_DB_PORT=3306
LOCAL_DB_NAME=dbgpt
```



