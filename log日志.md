## logback:

###### 1、组件介绍

Logback 建立在三个主要类之上 Logger： Appender 和 Layout 。
这三种类型的组件协同工作，使开发人员能够根据消息类型和级别记录消息，并在运行时控制这些消息的格式和报告位置。

###### 2、记录器

Loggers（记录器）控制日志的输出级别，规则是：只输出级别不低于设定级别的日志信息。以及引用 Appenders（输出器）。
Loggers（记录器）有一个根记录器位于记录器层次结构的顶部，它从一开始就是每个层次结构的一部分。
Loggers（记录器）之间有继承关系，例如名为 “com.foo” 的记录器是名为 “com.foo.Bar” 的记录器的父级。 同样 “java” 是 “java.util” 的父级和 “java.util.Vector” 的祖先。

###### 3、输出器

Appender（输出器）将日志记录请求打印到多个目的地，如控制台、文件、数据库等等。

###### 4、格式器

Layout（格式器）将事件转换成字符串，将日志信息格式化并输出。在 Logback 中 Layout 对象被封装在 encoder 中。

Pattern Layout（自定义格式器）常用转换符：

转换词	描述	性能
%c	在记录事件的起源处输出记录器的名称	
%C	输出发出日志请求的调用者的完全限定类名	差
%d	用于输出记录事件的日期，如 %d{yyyy-MM-dd HH:mm:ss.SSS}	
%F / file	输出发出记录请求的 Java 源文件的文件名	差
%L / line	输出发出记录请求的行号	差
%m / msg / message	输出与日志事件关联的应用程序提供的消息	
%M / method	输出发出记录请求的方法名称	差
%n	输出平台相关的行分隔符或字符	
%p / le / level	输出日志事件的级别	
%r / relative	输出从应用程序启动到创建日志记录事件所经过的毫秒数	
%t / thread	输出生成日志事件的线程的名称	
%20c	如果记录器名称的长度少于 20 个字符，则用空格填充	
%-20c	如果记录器名称的长度少于 20 个字符，则用空格填充右侧	
%.30c	如果记录器名称超过 30 个字符，则从头部截断	
%.-30c	如果记录器名称超过 30 个字符， 则从尾部截断

###### 5、扫描顺序

logback日志扫描顺序：（logback支持后缀名称为groovy与xml两种配置方式）

第一步：先在System Property系统配置文件中查找是否有logback.configurationFile对应的value

第二步：在classpath下寻找是否有logback.groovy文件

第三步：在classpath下寻找是否有logback-test.xml文件

第四步：在classpath下寻找是否有logback.xml文件

以上4步，只需要符合其中一步逻辑即可，具体代码实现可见ch.qos.logback.classic.util.ContextInitializer类的findURLOfDefaultConfigurationFile方法。

如果以上4步都步符合，logback会默认构造一个控制台输出日志，并会有默认的日志格式。

```java
<?xml version="1.0" encoding="UTF-8"?>
<!-- scan：程序运行时配置文件被修改，是否重新加载。true=重新加载；false=不重新加载；默认为true; -->
<!-- scanPeriod：监测配置文件被修改的时间间隔，scan属性必须设置为true才可生效；默认为1分钟，默认单位是毫秒； -->
<!-- debug：是否打印logback程序运行的日志信息。true=打印；false=不打印；默认为false; -->
<configuration scan="true" scanPeriod="60 seconds" debug="false">
    <!-- 自定义属性、相当于全局变量 -->
    <!-- name ：变量的名称，可以随意起名，建议简单明了； -->
    <!-- value：变量的值； -->
    <!-- 日志存放路径 -->
   <property name="log.path" value="logs/zhsd-fire" />
   <!-- 日志输出格式 -->
   <property name="log.pattern" value="%d{HH:mm:ss.SSS} [%thread] %-5level %logger{20} - [%method,%line] - %msg%n" />
	<!--appender标签中的name和class为必填属性 -->
    <!--name ：节点的名称 -->
    <!--class：全限定类名，ch.qos.logback.core.ConsoleAppender表示控制台输出 -->
    <!-- 控制台输出 -->
   <appender name="console" class="ch.qos.logback.core.ConsoleAppender">
      <encoder>
         <pattern>${log.pattern}</pattern>
      </encoder>
   </appender>

    <!-- 系统日志输出 -->
   <appender name="file_info" class="ch.qos.logback.core.rolling.RollingFileAppender">
       <file>${log.path}/info.log</file>
        <!-- 循环政策：基于时间创建日志文件 -->
      <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 日志文件名格式 -->
         <fileNamePattern>${log.path}/info.%d{yyyy-MM-dd}.log</fileNamePattern>
         <!-- 日志最大的历史 60天 -->
         <maxHistory>60</maxHistory>
      </rollingPolicy>
      <encoder>
         <pattern>${log.pattern}</pattern>
      </encoder>
      <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>INFO</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
   </appender>

    <appender name="file_error" class="ch.qos.logback.core.rolling.RollingFileAppender">
       <file>${log.path}/error.log</file>
        <!-- 循环政策：基于时间创建日志文件 -->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 日志文件名格式 -->
            <fileNamePattern>${log.path}/error.%d{yyyy-MM-dd}.log</fileNamePattern>
         <!-- 日志最大的历史 60天 -->
         <maxHistory>60</maxHistory>
        </rollingPolicy>
        <encoder>
            <pattern>${log.pattern}</pattern>
        </encoder>
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>ERROR</level>
         <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
         <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
    </appender>

    <appender name="file_modbus" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${log.path}/modbus.log</file>
        <!-- 循环政策：基于时间创建日志文件 -->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 日志文件名格式 -->
            <fileNamePattern>${log.path}/modbus.%d{yyyy-MM-dd}.log</fileNamePattern>
            <!-- 日志最大的历史 60天 -->
            <maxHistory>60</maxHistory>
        </rollingPolicy>
        <encoder>
            <pattern>${log.pattern}</pattern>
        </encoder>
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 过滤的级别 -->
            <level>DEBUG</level>
            <!-- 匹配时的操作：接收（记录） -->
            <onMatch>ACCEPT</onMatch>
            <!-- 不匹配时的操作：拒绝（不记录） -->
            <onMismatch>DENY</onMismatch>
        </filter>
    </appender>

    <!-- 系统模块日志级别控制  -->
   <logger name="com.zhsd" level="info" />
   <!-- Spring日志级别控制  -->
   <logger name="org.springframework" level="info" />
    <!-- modbus日志级别控制  -->
    <logger name="com.serotonin.modbus4j" level="debug" />

   <root level="info">
      <appender-ref ref="console" />
   </root>
            
    <!-- 配置异步输出器 -->
    <appender name="asyncAppender" class="ch.qos.logback.classic.AsyncAppender">
        <!-- 引用控制台输出器 -->
        <appender-ref ref="consoleAppender"/>
    </appender>
   	<!-- root： 但它位于logger体系中的最顶层。当一个类中的logger对象进行打印请求时，如果配置文件		中没有为该类单独指定日志对象，那么都会交给root根日志对象来完成； -->
	<!-- root： 节点中只有一个level属性，还可以单独指定日志输除目的地<apender-ref> -->
   	<!--系统操作日志-->
    <root level="info">
        <!-- 引用异步输出器 -->
        <appender-ref ref="asyncAppender"/>
        <appender-ref ref="file_info" />
        <appender-ref ref="file_error" />
        <appender-ref ref="file_modbus" />
    </root>
</configuration>

```

https://blog.csdn.net/bybeiya/category_11135166.html?spm=1001.2014.3001.5482



logger定义的包下的所有日志先到logger标签下，根据level级别进行过滤，（如果日志所在包没有logger标签，则默认日志过滤级别为root所指定日志级别），
将日志输出到其指定的appender-ref的appender里面，（如果没有指定appender-ref，则遵循root指定的appender-ref），再到appender进行日志的输出。
一般情况下appender要包含三个，即到elk的输出，控制台的输出，日志文件的记录。







先加载logback.xml-->bootstrap.yml-->logback-spring.xml,因为bootstrap.yml文件中有logback-spring.xml文件中使用到的服务名变量，所以要使得变量有值，得先加载bootstrap.yml文件后加载logback-spring.xml文件。





记录日志的几种方式：

1.使用注解结合AOP切面进行日志记录，取得入参出参响应时间。

2.使用监听如下：

```java
public static ApplicationContext publishEvent(Object event) {
    applicationContext.publishEvent(event);
    return applicationContext;
}
```

```java
public class LoginEvent extends ApplicationEvent {
    public LoginEvent(LoginStatusDTO source) {
        super(source);
    }
}
```

```java
@Component
@Slf4j
@RequiredArgsConstructor
public class LoginListener {
    private final LoginLogService loginLogService;
    private final UserService userService;
    private final DatabaseProperties databaseProperties;

    @Async
    @EventListener({LoginEvent.class})
    public void saveSysLog(LoginEvent event) {
        LoginStatusDTO loginStatus = (LoginStatusDTO) event.getSource();
        log.info("loginStatus={}", loginStatus);
        if (!MultiTenantType.NONE.eq(databaseProperties.getMultiTenantType()) && StrUtil.isEmpty(loginStatus.getTenant())) {
            log.warn("忽略记录登录日志:{}", loginStatus);
            return;
        }

        ContextUtil.setTenant(loginStatus.getTenant());
        if (LoginStatusDTO.Type.SUCCESS == loginStatus.getType()) {
            // 重置错误次数 和 最后登录时间
            this.userService.resetPassErrorNum(loginStatus.getId());

        } else if (LoginStatusDTO.Type.PWD_ERROR == loginStatus.getType()) {
            // 密码错误
            this.userService.incrPasswordErrorNumById(loginStatus.getId());
        }
        loginLogService.save(loginStatus.getId(), loginStatus.getAccount(), loginStatus.getUa(), loginStatus.getIp(), loginStatus.getLocation(), loginStatus.getDescription());
    }

}
```

