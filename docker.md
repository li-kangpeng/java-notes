# docker

#### Docker 启动容器后修改容器中的环境变量env

1.docker info，查看Docker Root Dir: /var/lib/docker

2.dokcer ps ,找到容器名，再docker [inspect](https://so.csdn.net/so/search?q=inspect&spm=1001.2101.3001.7020) [容器名], 获取容器的长id：container-id

3.docker stop [容器名]

4.vim /apps/docker/containers/[container-id]/config.v2.json
或者，vim /apps/docker/containers/[container-id]/config.json

5.systemctl daemon-reload

6.systemctl restart docker

#### 迁移容器（influx）

1. 查询原有镜像搭载目录

docker inspect --format='{{.Mounts}}' 容器名称

2. 挂载迁移

scp 挂载目录 目标服务器用户名@目标服务器ip:目标服务器目录

输入密码  完成挂载目录的拷贝

3. 迁移容器

先把数据库容器保存为镜像：

docker commit 容器ID 新镜像名

把镜像保存为压缩文件

docker save 新镜像ID > /目录/influxdb_20210705.tar

把压缩文件拷贝到新服务器

scp influxdb_20210705.tar root@IP地址:/home

在新服务器上载入镜像文件

docker load -i influxdb_20210705.tar

使用`docker tag`命令为加载的镜像指定名称和标签

docker tag <image-id> <new-image-name>:<tag>

在新服务器上加载数据库容器

docker run -idt --name 容器名称 -p 8086:8086 -v 挂载目录:/var/lib/influxdb 容器名称

国生项目为：

```tex
docker run -itd --name influxdb2 /
 -p 8086:8086 /
 -v /home/influxdb2/data:/var/lib/influxdb2 /
 -v /home/influxdb2/conf:/etc/influxdb2 /
 influxdb2
```

```
docker run -p 9000:9000 -p 9006:9006 \
 --name minio \
 -d --restart=always \
 -e "MINIO_ACCESS_KEY=admin" \
 -e "MINIO_SECRET_KEY=12345678" \
 -v /home/minio/data:/data \
 -v /home/minio/conf:/root/.minio \
  minio server \
 /data --console-address ":9006" --address ":9000"
```

#### dockerfile：

在编写Dockerfile时，有严格的格式需要遵循:

1. 第一行必须使用FROM指令指明所基于的镜像名称;

2. 之后使用MAINTAINER指令说明维护该镜像的用户信息;

3. 然后是镜像操作相关指令，如RUN指令。每运行一条指令，都会给基础镜像添加新的一 层。

4. 最后使用CMD指令指定启动容器时要运行的命令操作。

示例：

```sh
# 基础镜像
FROM  openjdk:8-jre
# author
MAINTAINER zhsd
# 挂载目录
VOLUME /home/zhsd
# 创建目录
RUN mkdir -p /home/zhsd
# 指定路径
WORKDIR /home/zhsd
# 复制jar文件到路径
COPY ./jar/zhsd-auth.jar /home/zhsd/zhsd-auth.jar
# 启动认证服务
ENTRYPOINT ["java","-jar","zhsd-auth.jar"]
```

|                  指令                  |                             含义                             |
| :------------------------------------: | :----------------------------------------------------------: |
|              FROM [镜像]               | 指定新镜像所基于的镜像，第一条指令必须为FROM指令，每创建一个镜像就需要一条FROM指令，例如centos:7。from有两层含义：①开启一个新的镜像②必须写的一行指令 |
|           MAINTAINER [名字]            |             说明新镜像的维护人信息（可写可不写）             |
|                RUN命令                 | 每一条RUN后面跟一条命令，在所基于的镜像上执行命令，并提交到新的镜像中，RUN必须大写 |
| CMD [“要运行的程序”，“参数1”、“参数2”] | 指定启动容器时需要运行的命令或者脚本，Dockerfile只能有一条CMD命令，如果指定多条则只能执行最后一条，“bin/bash”也是一条CMD,并且会覆盖image镜像里面的cmd。 |
|            EXPOSE [端口号]             | 指定新镜像加载到Docker时要开启的端口暴露端口，就是这个容器暴露出去的端口号。 |
|        ENV [环境变量] [变量值]         | 设置一个环境变量的值，会被后面的RUN使用。容器可以根据自己的需求创建时传入环境变量，镜像不可以。 |
|   ADD [源文件/目录] [目标文件/目录]    | ①将源文件复制到目标文件，源文件要与Dockerfile位于相同目录中，②或者是一个URL，③若源文件是压缩包则会将其解压缩 |
|   COPY [源文件/目录] [目标文件/目录]   | 将本地主机上的文件/目录复制到目标地点，源文件/目录要与Dockerfile在相同的目录中，copy只能用于复制，add复制的同时，如果复制的对象是压缩包，ADD还可以解压，copy比add节省资源 |
|            VOLUME [“目录”]             | 在容器中创建一个挂载点，简单来说就是-v，指定镜像的目录挂载到宿主机上。 |
|           USER [用户名/UID]            |                     指定运行容器时的用户                     |
|             WORKDIR [路径]             | 为后续的RUN、CMD、ENTRYPOINT指定工作目录，相当于是一个临时的"CD"，否则需要使用绝对路径，例如workdir /opt。移动到opt目录，并在这下面的指令都是在opt下执行。 |
|             ONBUILD [命令]             | 指定所生成的镜像作为一个基础镜像时所要运行的命令*（是一种优化）** |
|              HEALTHCHECK               |                           健康检查                           |


	
	
	
	
	
	
	
	
	
	
	
	

