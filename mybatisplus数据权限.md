#### 1.登陆时向loginUser注入数据权限

```java
        List<SysRole> roles = loginUser.getUser().getRoles();
        List<Long> roleIds = roles.stream().map(SysRole::getRoleId).distinct().collect(Collectors.toList());
        if(StringUtils.isNotEmpty(roleIds)){
            List<String> dataScopes = roles.stream().map(SysRole::getDataScope).distinct().collect(Collectors.toList());
            if(!dataScopes.contains(DATA_SCOPE_ALL)){
                loginUser.setDataScope(DATA_SCOPE_CUSTOM);
                List<SysRoleWarship> roleWarships = roleWarshipMapper.selectList(new LambdaQueryWrapper<SysRoleWarship>().in(SysRoleWarship::getRoleId,roleIds));
                loginUser.setWarshipIds(roleWarships.stream().map(SysRoleWarship::getWarshipId).distinct().collect(Collectors.toList()));
            }else {
                loginUser.setDataScope(DATA_SCOPE_ALL);
            }
        }
```

#### 2.添加数据权限注解

```java
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DataScopePlus {
 
    /**
     * 是否生效，默认true-生效
     */
    boolean enabled() default true;
 
    /**
     * 表别名 自定义sql语句有表别名就设置
     */
    String tableAlias() default "";
 
    /**
     * 部门限制范围的字段名称
     */
    String columnName();
 
    /**
     * 本人限制范围的字段名称
     */
    String oneselfScopeName() default "create_by";
 
}
```

#### 3.添加mybatisplus扫描包，确定mapper文件夹下哪个方法含有该注解

```java
@Slf4j
public class PlusDataPermissionHandler {

    /**
     * 方法或类(名称) 与 注解的映射关系缓存
     */
    private final Map<String, DataScopePlus> dataPermissionCacheMap = new ConcurrentHashMap<>();

    /**
     * 构造方法，扫描指定包下的 Mapper 类并初始化缓存
     *
     * @param mapperPackage Mapper 类所在的包路径
     */
    public PlusDataPermissionHandler(String mapperPackage) {
        scanMapperClasses(mapperPackage);
    }

    /**
     * 获取数据过滤条件的 SQL 片段
     *
     * @param where             原始的查询条件表达式
     * @param mappedStatementId Mapper 方法的 ID
     * @param isSelect          是否为查询语句
     * @return 数据过滤条件的 SQL 片段
     */
    public Expression getSqlSegment(Expression where, String mappedStatementId, boolean isSelect) {
        // 获取数据权限配置
        DataScopePlus dataPermission = getDataPermission(mappedStatementId);
        if(!dataPermission.enabled())
            return where;
        // 获取当前登录用户信息
        LoginUser currentUser = SecurityUtils.getLoginUser();
        if(isSelect)
        // 构造数据过滤条件的 SQL 片段
            return buildDataFilter(currentUser, dataPermission, where);
        return where;
    }

    /**
     * 构建数据过滤条件的 SQL 语句
     *
     * @param dataScopePlus 数据权限注解
     * @param where       当前sql条件
     * @return 构建的数据过滤条件的 SQL 语句
     */
    private Expression buildDataFilter(LoginUser currentUser, DataScopePlus dataScopePlus, Expression where) {
        // 如果是超级管理员，则不过滤数据
        if (StringUtils.isNotNull(currentUser) && !currentUser.getUser().isAdmin()) {
            String dataScope = currentUser.getDataScope();
            if (DataScopeAspect.DATA_SCOPE_ALL.equals(dataScope)) {
                return where;
            }
            List<String> permissions = currentUser.getWarshipIds();
            if (CollectionUtil.isEmpty(permissions)) {
                log.warn("[dataPermissionHandler] 当前用户无注明数据权限, 可能是没有登陆, 也可能是超管, userId:[{}]", currentUser.getUserId());
                return where;
            }
            ItemsList itemsList = new ExpressionList(permissions.stream().map(k -> new StringValue(String.valueOf(k))).collect(Collectors.toList()));
            String column = getAliasColumn(dataScopePlus);
            InExpression inExpression = new InExpression(new Column(column), itemsList);
            if (null == where) {
                // 不存在 where 条件
                return new Parenthesis(inExpression);
            } else {
                // 存在 where 条件 and 处理
                return new AndExpression(where,new Parenthesis(inExpression));
            }
        }
        return where;
    }

    /**
     * 扫描指定包下的 Mapper 类，并查找其中带有特定注解的方法或类
     *
     * @param mapperPackage Mapper 类所在的包路径
     */
    private void scanMapperClasses(String mapperPackage) {
        // 创建资源解析器和元数据读取工厂
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
        // 将 Mapper 包路径按分隔符拆分为数组
        String[] packagePatternArray = StringUtils.splitPreserveAllTokens(mapperPackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);
        String classpath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX;
        try {
            for (String packagePattern : packagePatternArray) {
                // 将包路径转换为资源路径
                String path = ClassUtils.convertClassNameToResourcePath(packagePattern);
                // 获取指定路径下的所有 .class 文件资源
                Resource[] resources = resolver.getResources(classpath + path + "/*.class");
                for (Resource resource : resources) {
                    // 获取资源的类元数据
                    ClassMetadata classMetadata = factory.getMetadataReader(resource).getClassMetadata();
                    // 获取资源对应的类对象
                    Class<?> clazz = Resources.classForName(classMetadata.getClassName());
                    // 查找类中的特定注解
                    findAnnotation(clazz);
                }
            }
        } catch (Exception e) {
            log.error("初始化数据安全缓存时出错:{}", e.getMessage());
        }
    }

    /**
     * 在指定的类中查找特定的注解 DataScopePlus，并将带有这个注解的方法或类存储到 dataPermissionCacheMap 中
     *
     * @param clazz 要查找的类
     */
    private void findAnnotation(Class<?> clazz) {
        DataScopePlus dataPermission;
        for (Method method : clazz.getMethods()) {
            if (method.isDefault() || method.isVarArgs()) {
                continue;
            }
            String mappedStatementId = clazz.getName() + "." + method.getName();
            if (AnnotationUtil.hasAnnotation(method, DataScopePlus.class)) {
                dataPermission = AnnotationUtil.getAnnotation(method, DataScopePlus.class);
                dataPermissionCacheMap.put(mappedStatementId, dataPermission);
            }
        }
        if (AnnotationUtil.hasAnnotation(clazz, DataScopePlus.class)) {
            dataPermission = AnnotationUtil.getAnnotation(clazz, DataScopePlus.class);
            dataPermissionCacheMap.put(clazz.getName(), dataPermission);
        }
    }

    /**
     * 根据映射语句 ID 或类名获取对应的 DataScopePlus 注解对象
     *
     * @param mapperId 映射语句 ID
     * @return DataScopePlus 注解对象，如果不存在则返回 null
     */
    public DataScopePlus getDataPermission(String mapperId) {
        // 检查缓存中是否包含映射语句 ID 对应的 DataScopePlus 注解对象
        if (dataPermissionCacheMap.containsKey(mapperId)) {
            return dataPermissionCacheMap.get(mapperId);
        }
        // 如果缓存中不包含映射语句 ID 对应的 DataScopePlus 注解对象，则尝试使用类名作为键查找
        String clazzName = mapperId.substring(0, mapperId.lastIndexOf("."));
        if (dataPermissionCacheMap.containsKey(clazzName)) {
            return dataPermissionCacheMap.get(clazzName);
        }
        return null;
    }

    /**
     * 检查给定的映射语句 ID 是否有效，即是否能够找到对应的 DataScopePlus 注解对象
     *
     * @param mapperId 映射语句 ID
     * @return 如果找到对应的 DataScopePlus 注解对象，则返回 false；否则返回 true
     */
    public boolean invalid(String mapperId) {
        return getDataPermission(mapperId) == null;
    }


    /**
     * 获取字段名称
     *
     * @param dataScopePlus annotation
     * @return 字段名
     */
    private String getAliasColumn(DataScopePlus dataScopePlus) {
        String column = dataScopePlus.columnName();
        if (StrUtil.isNotBlank(dataScopePlus.tableAlias())) {
            column = StrUtil.builder().append(dataScopePlus.tableAlias()).append(StringPool.DOT).append(dataScopePlus.columnName()).toString();
        }
        return column;
    }
```

#### 4.添加配置文件mapper扫描包位置（mapperPackage）

```yml
# MyBatis配置
mybatis-plus:
  mapperPackage: com.ship.**.mapper
  # 搜索指定包别名
  typeAliasesPackage: com.ship.**.domain
  # 配置mapper的扫描，找到所有的mapper.xml映射文件
  mapperLocations: classpath*:mapper/**/*Mapper.xml
  # 加载全局的配置文件
  configLocation: classpath:mybatis/mybatis-config.xml
  global-config:
    db-config:
      logic-delete-field: del_flag # 全局逻辑删除字段名
      logic-delete-value: 1 # 逻辑已删除值
      logic-not-delete-value: 0 # 逻辑未删除值
      id-type: auto #主键生成策略
```

#### 5.添加数据权限拦截器

```java
@Slf4j
public class PlusDataPermissionInterceptor extends BaseMultiTableInnerInterceptor implements InnerInterceptor {

    private final PlusDataPermissionHandler dataPermissionHandler;

    /**
     * 构造函数，初始化 PlusDataPermissionHandler 实例
     *
     * @param mapperPackage 扫描的映射器包
     */
    public PlusDataPermissionInterceptor(String mapperPackage) {
        this.dataPermissionHandler = new PlusDataPermissionHandler(mapperPackage);
    }

    /**
     * 在执行查询之前，检查并处理数据权限相关逻辑
     *
     * @param executor      MyBatis 执行器对象
     * @param ms            映射语句对象
     * @param parameter     方法参数
     * @param rowBounds     分页对象
     * @param resultHandler 结果处理器
     * @param boundSql      绑定的 SQL 对象
     * @throws SQLException 如果发生 SQL 异常
     */
    @Override
    public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        // 检查是否需要忽略数据权限处理
        if (InterceptorIgnoreHelper.willIgnoreDataPermission(ms.getId())) {
            return;
        }
        // 检查是否缺少有效的数据权限注解
        if (dataPermissionHandler.invalid(ms.getId())) {
            return;
        }
        // 解析 sql 分配对应方法
        PluginUtils.MPBoundSql mpBs = PluginUtils.mpBoundSql(boundSql);
        mpBs.sql(parserSingle(mpBs.sql(), ms.getId()));
    }

    /**
     * 在准备 SQL 语句之前，检查并处理更新和删除操作的数据权限相关逻辑
     *
     * @param sh                 MyBatis StatementHandler 对象
     * @param connection         数据库连接对象
     * @param transactionTimeout 事务超时时间
     */
    @Override
    public void beforePrepare(StatementHandler sh, Connection connection, Integer transactionTimeout) {
        PluginUtils.MPStatementHandler mpSh = PluginUtils.mpStatementHandler(sh);
        MappedStatement ms = mpSh.mappedStatement();
        // 获取 SQL 命令类型（增、删、改、查）
        SqlCommandType sct = ms.getSqlCommandType();

        // 只处理更新和删除操作的 SQL 语句
        if (sct == SqlCommandType.UPDATE || sct == SqlCommandType.DELETE) {
            if (InterceptorIgnoreHelper.willIgnoreDataPermission(ms.getId())) {
                return;
            }
            // 检查是否缺少有效的数据权限注解
            if (dataPermissionHandler.invalid(ms.getId())) {
                return;
            }
            PluginUtils.MPBoundSql mpBs = mpSh.mPBoundSql();
            mpBs.sql(parserMulti(mpBs.sql(), ms.getId()));
        }
    }

    /**
     * 处理 SELECT 查询语句中的 WHERE 条件
     *
     * @param select SELECT 查询对象
     * @param index  查询语句的索引
     * @param sql    查询语句
     * @param obj    WHERE 条件参数
     */
    @Override
    protected void processSelect(Select select, int index, String sql, Object obj) {
        SelectBody selectBody = select.getSelectBody();
        if (selectBody instanceof PlainSelect) {
            this.setWhere((PlainSelect) selectBody, (String) obj);
        } else if (selectBody instanceof SetOperationList) {
            SetOperationList setOperationList = (SetOperationList)selectBody;
            List<SelectBody> selectBodyList = setOperationList.getSelects();
            selectBodyList.forEach(s -> this.setWhere((PlainSelect) s, (String) obj));
        }
    }

    /**
     * 处理 UPDATE 语句中的 WHERE 条件
     *
     * @param update UPDATE 查询对象
     * @param index  查询语句的索引
     * @param sql    查询语句
     * @param obj    WHERE 条件参数
     */
    @Override
    protected void processUpdate(Update update, int index, String sql, Object obj) {
        Expression sqlSegment = dataPermissionHandler.getSqlSegment(update.getWhere(), (String) obj, false);
        if (null != sqlSegment) {
            update.setWhere(sqlSegment);
        }
    }

    /**
     * 处理 DELETE 语句中的 WHERE 条件
     *
     * @param delete DELETE 查询对象
     * @param index  查询语句的索引
     * @param sql    查询语句
     * @param obj    WHERE 条件参数
     */
    @Override
    protected void processDelete(Delete delete, int index, String sql, Object obj) {
        Expression sqlSegment = dataPermissionHandler.getSqlSegment(delete.getWhere(), (String) obj, false);
        if (null != sqlSegment) {
            delete.setWhere(sqlSegment);
        }
    }

    /**
     * 设置 SELECT 语句的 WHERE 条件
     *
     * @param plainSelect       SELECT 查询对象
     * @param mappedStatementId 映射语句的 ID
     */
    protected void setWhere(PlainSelect plainSelect, String mappedStatementId) {
        Expression sqlSegment = dataPermissionHandler.getSqlSegment(plainSelect.getWhere(), mappedStatementId, true);
        if (null != sqlSegment) {
            plainSelect.setWhere(sqlSegment);
        }
    }

    /**
     * 构建表达式，用于处理表的数据权限
     *
     * @param table        表对象
     * @param where        WHERE 条件表达式
     * @param whereSegment WHERE 条件片段
     * @return 构建的表达式
     */
    @Override
    public Expression buildTableExpression(Table table, Expression where, String whereSegment) {
        // 只有新版数据权限处理器才会执行到这里
        final MultiDataPermissionHandler handler = (MultiDataPermissionHandler) dataPermissionHandler;
        return handler.getSqlSegment(table, where, whereSegment);
    }
}

```

#### 6.mybatisplus加入该拦截器（需要在分页拦截器之前）

```java
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor()
    {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PlusDataPermissionInterceptor(SpringUtil.getProperty("mybatis-plus.mapperPackage")));
        // 分页插件
        interceptor.addInnerInterceptor(paginationInnerInterceptor());
        // 乐观锁插件
        interceptor.addInnerInterceptor(optimisticLockerInnerInterceptor());

        // 阻断插件
        //interceptor.addInnerInterceptor(blockAttackInnerInterceptor());
        return interceptor;
    }
```

#### 7.在mapper方法上加入数据权限注解

```java
    @DataScopePlus(columnName = "demand_warship")
    Page<ZcDemand> selectPage(@Param("page")Page<ZcDemand> page, @Param("ew") Wrapper<ZcDemand> wrapper);
```

```xml
  <select id="selectPage" resultMap="BaseResultMap">
    select
    <if test="ew.getSqlSelect != null">
      ${ew.getSqlSelect}
    </if>
    <if test="ew.getSqlSelect == null">
      <include refid="Base_Column_List"/>
    </if>
    from zc_demand
    ${ew.getCustomSqlSegment}
  </select>
```

