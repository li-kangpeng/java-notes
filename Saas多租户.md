## Saas多租户

```
| 租户模式 | 描述 | 优点 | 缺点  |
|---|---|---|---|
| NONE(非租户模式) | 没有租户 | 简单、适合独立系统 | 缺少租户系统的优点 |
| COLUMN(字段模式) | 租户共用一个数据库，在业务表中增加字段来区分 | 简单、不复杂、开发无感知 | 数据隔离性差、安全性差、数据备份和恢复困难、 |
| SCHEMA(独立schema) | 每个租户独立一个 数据库(schema)，执行sql时，动态在表名前增加schema | 简单、开发无感知、数据隔离性好 | 配置文件中必须配置数据库的root账号、不支持复杂sql和 sql嵌套自定义函数 |
| DATASOURCE(独立数据源) | 每个租户独立一个 数据库(数据源)，执行代码时，动态切换数据源 | 可独立部署数据库，数据隔离性好、扩展性高、故障影响小 | 相对复杂、开发需要注意切换数据源时的事务问题、需要较多的数据库 |
```

[oracle](https://so.csdn.net/so/search?q=oracle&spm=1001.2101.3001.7020)数据库：在oracle中一个数据库可以具有多个用户，那么一个用户一般对应一个Schema，表都是建立在Schema中的,（可以简单的理解：在oracle中一个用户一套数据库表）

![img](https://img-blog.csdnimg.cn/5cd1351b6e284ff6b694de498998c66a.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBATGVvbl9KaW5oYWlfU3Vu,size_20,color_FFFFFF,t_70,g_se,x_16)

mysql数据库：mysql数据中的schema比较特殊，并不是数据库的下一级，而是等同于数据库。比如执行create schema test 和执行create database test效果是一模一样的

![img](https://img-blog.csdnimg.cn/21248af335fa4566961185a33fe0466f.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBATGVvbl9KaW5oYWlfU3Vu,size_20,color_FFFFFF,t_70,g_se,x_16)

##### COLUMN(字段模式) 

1.zhsd-common-core模块下添加内容

![image-20230316092221777](C:/Users/admin/AppData/Roaming/Typora/typora-user-images/image-20230316092221777.png)


