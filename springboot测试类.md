1.pom依赖

```xml
<dependency> 
	<groupId>org.springframework.boot</groupId> 
	<artifactId>spring-boot-starter-test</artifactId> 
	<scope>test</scope> 
</dependency> 
```

#### 创建类和方法

创建类对应单测类的快捷方法：只需将双击这个类，鼠标右键，然后选择go to到Test。

![image-20231215140624148](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231215140624148.png)

选择要测试的方法，同时，可以选择JUnit版本等，点击OK

![image-20231215140749792](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231215140749792.png)

#### Controller层单测

###### 被测代码如下：

```java
@Slf4j
@RestController
@RequestMapping("/demo")
@Api(value = "DemoController", tags = "Demo管理模块")
public class DemoController implements DemoApi {
    /**
     * service
     */
    @Autowired
    private DemoService demoService;
 
 
    @Override
    @ApiOperation(value = "新增", notes = "新增")
    @PostMapping("/create")
    public Boolean add(@RequestHeader("appCode") String appCode,
                                     @RequestBody DemoDTO demoDTO) {
        boolean addFlag = demoService.add(demoDTO);
        if (addFlag) {
            // 刷新资源
          demoService.refreshMap(appCode);
        }
        return addFlag;
    }
 
 
    @Override
    @ApiOperation(value = "修改", notes = "修改")
    @PostMapping("/update")
    public Boolean update(@RequestHeader("appCode") String appCode,
                                        @RequestBody DemoDTO demoDTO) {
      
        boolean addFlag = demoService.update(demoDTO);
        if (addFlag) {
            // 刷新资源
          demoService.refreshMap(appCode);
        }
        return addFlag;
    }
 
 
    @Override
    @ApiOperation(value = "删除", notes = "删除")
    @DeleteMapping("/delById")
    public Boolean deleteById(@RequestHeader("appCode") String appCode, @RequestParam Long id) {
        boolean deleteFlag = demoService.deleteById(id);
        if (deleteFlag) {
            // 刷新资源
          demoService.refreshMap(appCode);
        }
        return addFlag;
    }
 
 
    @Override
    @ApiOperation(value = "列表", notes = "列表")
    @GetMapping("/list")
    public List<DemoVO> list() {
        return demoService.list();
    }
 }
```

###### 测试代码：

1. 单独组件测试，无需使用@SpringBootTest进行整体上下文启动，使用@RunWith(SpringRunner.class)运行测试用例。

2. 使用@InjectMock注入controller类。

3. 使用@Mock注解mock service类。

   ```java
   @RunWith(SpringRunner.class)
   public class DemoControllerTest {
       /**
        * mock mvc
        */
       private MockMvc mockMvc;
    
    
       /**
        * 注入实例
        */
       @InjectMocks
       private DemoController demoController;
    
    
       /**
        * service mock
        */
       @Mock
       private DemoService demoService;
       
       /**
        * appCode
        */
       private String appCode;
    
    
       /**
        * before设置
        */
       @Before
       public void setUp() {
           //初始化带注解的对象
           MockitoAnnotations.openMocks(this);
           //构造mockmvc
           mockMvc = MockMvcBuilders.standaloneSetup(demoController).build();
           //appCode
           appCode = "AppCode_test";
       }
    
    
       /**
        * 测试testAdd
        */
       @Test
       public void testAdd() throws Exception {
           //构建dto
           DemoDTO demoDTO = new DemoDTO();
           //setId
           demoDTO.setId(-1L);
           //setName
           demoDTO.setName("test");
           //mock service方法
      		PowerMockito.when(demoService.add(demoDTO)).thenReturn(true);
           //构造body
           String body = JSONObject.toJSONString(demoDTO);
           //执行mockmvc
           this.mockMvc.perform(MockMvcRequestBuilders.post("/demo/create")
                   //传参
                   .header("appCode", appCode).content(body).contentType(MediaType.APPLICATION_JSON_VALUE))
                   //mock返回
                   .andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
       }
    
    
       /**
        * 测试testUpdate
        */
       @Test
       public void testUpdate() throws Exception {
             //构建dto
           DemoDTO demoDTO = new DemoDTO();
           //setId
           demoDTO.setId(-1L);
           //setName
           demoDTO.setName("test");
           //mock service方法
           PowerMockito.when(demoService.update(demoDTO)).thenReturn(true);
           //构造body
           String body = JSONObject.toJSONString(demoDTO);
           //执行mockmvc
           this.mockMvc.perform(MockMvcRequestBuilders.post("/demo/update")
                   //传参
                   .header("appCode", appCode).content(body).contentType(MediaType.APPLICATION_JSON_VALUE))
                   //mock返回
                   .andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
       }
    
    
       /**
        * 测试testDelete
        */
       @Test
       public void testDelete() throws Exception {
           //Id
           Long id = 1000L;
           //mock service方法
           PowerMockito.when(demoService.deleteById(id)).thenReturn(true);
           //执行mockmvc 方法一
   //        this.mockMvc.perform(MockMvcRequestBuilders.delete("/demo/delById?id={id}",id)
   //                //传参
   //                .header("appCode", appCode).contentType(MediaType.APPLICATION_JSON_VALUE))
   //                //mock返回
   //.andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
   //    }
   		//方法二
           this.mockMvc.perform(MockMvcRequestBuilders.delete("/demo/delById")
                   //传参
                   .header("appCode", appCode).param("id", "1000").contentType(MediaType.APPLICATION_JSON_VALUE))
                   //mock返回
                   .andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
       }
    
    
       /**
        * 测试testList
        */
       @Test
       public void testList() throws Exception {
           this.mockMvc.perform(MockMvcRequestBuilders.get("/demo/list")
                   //传参
                   .contentType(MediaType.APPLICATION_JSON_VALUE))
                   //mock返回
                   .andExpect(status().isOk()).andDo(MockMvcResultHandlers.print()).andReturn();
       }
   }
   ```

   #### Service层单测

###### 被测代码：

```java
@Service 
public class MenuService { 
    @Autowired 
    private MenuMapper menuMapper; 
    
    @Transactional(readOnly = true) 
    public List<Menu> listMenus() { 
        final List<Menu> result = menuMapper.list(); 
        return result; 
    }
} 
```

###### 测试代码：

通过 @TestConfiguration 创建一个测试用配置，该配置中提供了一个 MenuService Bean的声明。
该注解的使用有以下几个注意点：被注解的类须是 static 的，且不能是 private 的。建议用在使用在内部类上，否则所定义的 Bean 将不会被自动加载。须通过以下方式之一进行加载

```java
@Import(MenuServiceTestConfig.class)
@ContextConfiguration(classes =MenuServiceTestConfig.class)
@Autowired
//此处通过 @Autowired 自动注入的是上面通过 @TestConfiguration 声明的 Bean。
@RunWith(SpringRunner.class) 
public class MenuServiceTest { 
    @TestConfiguration 
    static class MenuServiceTestConfig { 
        @Bean 
        public MenuService mockMenuService() { 
            return new MenuService(); 
        } 
    }
    @Autowired 
    private MenuService MenuService; 
    @MockBean 
    private MenuMapper MenuMapper; 
    @Test 
    public void listMenus() { 
        List<Menu> menus = new ArrayList<Menu>() {{ 
            this.add(new Menu()); 
        }}; 
        Mockito.when(menuMapper.list()).thenReturn(menus); 
        List<Menu> result = menuService.listMenus(); 
        Assertions.assertThat(result.size()).isEqualTo(menus.size()); 
    } 
}
```

可参考：https://juejin.cn/post/7222577873793138747