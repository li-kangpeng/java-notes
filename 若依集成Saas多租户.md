

## 若依集成Saas多租户

##### 1.修改文件

具体参考SdStandard项目的tenant分支。（不想说明。。。。改动比较大）。

![image-20230321142139777](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20230321142139777.png)

##### 2.启动项目在菜单管理添加新页面，配置如下

![image-20230321142337398](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20230321142337398.png)

![image-20230321143505642](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20230321143505642.png)

![image-20230321143536430](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20230321143536430.png)

##### 3.修改nacos配置文件

```yml
# mybatis配置
mybatis-plus:
    # 搜索指定包别名
    typeAliasesPackage: com.zhsd.system,com.zhsd.common.datascope,com.zhsd.common.core
  # 配置mapper的扫描，找到所有的mapper.xml映射文件
    mapperLocations: classpath:mapper/*.xml, classpath*:mapper/**/*.xml,classpath*:mapper/*.xml
```

```yml
zhsd:
  tenant:
    column: tenant_id
    ignore-tables:
      - sys_dict_type
      - sys_dict_data
      - sys_menu
      - sys_config
      - sys_tenant
      - sys_tenant_package
```

ignore-tables:为需要忽略多租户的表名，按需进行添加。

##### 4.修改数据库

对于需要数据隔离的表添加列tenant_id

目前若依初始化需要添加列tenant_id的表如下：

sys_dept、sys_loginfor、sys_oper_log、sys_post、sys_role（还需要添加admin_role(varchar 1)是否管理员角色（0 不是 1是））、sys_role_dept、sys_role_menu、sys_user、sys_user_post、sys_user_role,对于初始超级管理员用户tenant_id均设置为9999即可。