## RocketMQ

#### MQ简介：

MQ，Message Queue，是一种提供消息队列服务的中间件，也称为消息中间件，是一套提供了消息生 产、存储、消费全过程API的软件系统。消息即数据。一般消息的体量不会很大。

#### MQ作用：

限流削峰：MQ可以将系统的超量请求暂存其中，以便系统后期可以慢慢进行处理，从而避免了请求的丢失或系统 被压垮。

异步解耦：上游系统调用下游系统若为同步，则会大大降低系统的吞吐量与并发量，且耦合度较高，不宜拓展。使用MQ后，上游将消息发给MQ，下游从MQ中取消息进行操作，大大降低了耦合度，且上游发消息后即结束，可保证系统的并发度。

#### MQ产品对比：

​						ACTIVEMQ  		RABBITMQ 			KAFKA 				ROCKETMQ 

开发语言 			Java 						ErLang 					Java 						Java 

单机吞吐量 		万级 						万级 					十万级 						十万级 

Topic						 -							 - 				百级Topic时会影   	千级Topic时会影

​																					响系统吞吐量 			响系统吞吐 量

 社区活跃度 		低 							高 						高 								高

#### NameServer：

NameServer是一个Broker与Topic路由的注册中心，支持Broker的动态注册与发现。

包括两个功能： 

Broker管理：接受Broker集群的注册信息并且保存下来作为路由信息的基本数据；提供心跳检测 机制，检查Broker是否还存活。 NameServer中有⼀个定时任务，每隔10秒就会扫描⼀次Broker表，查看每一个Broker的最新心跳时间 戳距离当前时间是否超过120秒，如果超过，则会判定Broker失效，然后将其从Broker列表中剔除。

路由信息管理：每个NameServer中都保存着Broker集群的整个路由信息和用于客户端查询的队列 信息。Producer和Conumser通过NameServer可以获取整个Broker集群的路由信息，从而进行消 息的投递和消费。

客户端（Producer与Consumer）NameServer选择策略：首先采用的是随机策略进行的选择，失败后采用的是轮询策略。

#### Broker：

Broker充当着消息中转角色，负责存储消息、转发消息。Broker在RocketMQ系统中负责接收并存储从 生产者发送来的消息，同时为消费者的拉取请求作准备。Broker同时也存储着消息相关的元数据，包括 消费者组消费进度偏移offset、主题、队列等。

为了增强Broker性能与吞吐量，Broker一般都是以集群形式出现的。Broker节点集群是一个主从集群，即集群中具有Master与Slave两种角色。Master负责处理读写操作请 求，Slave负责对Master中的数据进行备份。当Master挂掉了，Slave则会自动切换为Master去工作。所 以这个Broker集群是主备集群。一个Master可以包含多个Slave，但一个Slave只能隶属于一个Master。 Master与Slave 的对应关系是通过指定相同的BrokerName、不同的BrokerId 来确定的。BrokerId为0表 示Master，非0表示Slave。每个Broker与NameServer集群中的所有节点建立长连接，定时注册Topic信 息到所有NameServer。

#### 工作流程：

1）启动NameServer，NameServer启动后开始监听端口，等待Broker、Producer、Consumer连接。 

2）启动Broker时，Broker会与所有的NameServer建立并保持长连接，然后每30秒向NameServer定时 发送心跳包。 

3）发送消息前，可以先创建Topic，创建Topic时需要指定该Topic要存储在哪些Broker上，当然，在创 建Topic时也会将Topic与Broker的关系写入到NameServer中。不过，这步是可选的，也可以在发送消 息时自动创建Topic。 

4）Producer发送消息，启动时先跟NameServer集群中的其中一台建立长连接，并从NameServer中获 取路由信息，即当前发送的Topic消息的Queue与Broker的地址（IP+Port）的映射关系。然后根据算法 策略从队选择一个Queue，与队列所在的Broker建立长连接从而向Broker发消息。当然，在获取到路由 信息后，Producer会首先将路由信息缓存到本地，再每30秒从NameServer更新一次路由信息。

 5）Consumer跟Producer类似，跟其中一台NameServer建立长连接，获取其所订阅Topic的路由信息， 然后根据算法策略从路由信息中获取到其所要消费的Queue，然后直接跟Broker建立长连接，开始消费 其中的消息。Consumer在获取到路由信息后，同样也会每30秒从NameServer更新一次路由信息。不过 不同于Producer的是，Consumer还会向Broker发送心跳，以确保Broker的存活状态。

####  消息的生产过程：

Producer发送消息之前，会先向NameServer发出获取消息Topic的路由信息的请求 

NameServer返回该Topic的路由表及Broker列表 

Producer根据代码中指定的Queue选择策略，从Queue列表中选出一个队列，用于后续存储消息 

Produer对消息做一些特殊处理，例如，消息本身超过4M，则会对其进行压缩 

Producer向选择出的Queue所在的Broker发出RPC请求，将消息发送到选择出的Queue

####  Queue选择算法：

对于无序消息，其Queue选择算法，也称为消息投递算法，常见的有两种：

轮询算法：默认选择算法。该算法保证了每个Queue中可以均匀的获取到消息。

最小投递延迟算法：该算法会统计每次消息投递的时间延迟，然后根据统计出的结果将消息投递到时间延迟最小的Queue。 如果延迟相同，则采用轮询算法投递。该算法可以有效提升消息的投递性能。

#### 消息的消费：

消费者从Broker中获取消息的方式有两种：pull拉取方式和push推动方式。消费者组对于消息消费的模 式又分为两种：集群消费Clustering和广播消费Broadcasting。

拉取式消费：

 Consumer主动从Broker中拉取消息，主动权由Consumer控制。一旦获取了批量消息，就会启动消费过 程。不过，该方式的实时性较弱，即Broker中有了新的消息时消费者并不能及时发现并消费。 由于拉取时间间隔是由用户指定的，所以在设置该间隔时需要注意平稳：间隔太短，空请求比 例会增加；间隔太长，消息的实时性太差 

推送式消费 ：

该模式下Broker收到数据后会主动推送给Consumer。该获取方式一般实时性较高。 该获取方式是典型的发布-订阅模式，即Consumer向其关联的Queue注册了监听器，一旦发现有新的 消息到来就会触发回调的执行，回调方法是Consumer去Queue中拉取消息。而这些都是基于Consumer 与Broker间的长连接的。长连接的维护是需要消耗系统资源的。

广播消费：

广播消费模式下，相同Consumer Group的每个Consumer实例都接收同一个Topic的全量消息。即每条 消息都会被发送到Consumer Group中的每个Consumer。

集群消费：

集群消费模式下，相同Consumer Group的每个Consumer实例平均分摊同一个Topic的消息。即每条消 息只会被发送到Consumer Group中的某个Consumer。

#### 订阅关系的一致性：

订阅关系的一致性指的是，同一个消费者组（Group ID相同）下所有Consumer实例所订阅的Topic与 Tag及对消息的处理逻辑必须完全一致。否则，消息消费的逻辑就会混乱，甚至导致消息丢失。

#### 消息失败重试：

消费者批量消费数量设为1，保证消费失败只对这一条消息进行重试

```
consumer.setConsumeMessageBatchMaxSize(1);
```

重试次数 与上次重试的间隔时间（默认重试16次，也就是说这条消息默认重试最长时间长达4小时46分）

 1 10秒	2 30秒	3 1分钟	4 2分钟	5 3分钟	6 4分钟	7 5分钟	8 6分钟	9 7分钟	10 8分钟

 11 9分钟  12 10分钟  13 20分钟  14 30分钟  15 1小时  16 2小时。

```
consumer.setMaxReconsumeTimes(3);
```

无序消息集群消费下的重试消费：

```java
consumer.registerMessageListener(new MessageListenerConcurrently() {
    // 一旦broker中有了其订阅的消息就会触发该方法的执行，
    // 其返回值为当前consumer消费的状态
    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
        // 逐条消费消息
        for (MessageExt msg : msgs) {
            try {
                //模拟消费失败
                if(Integer.parseInt(new String(msg.getBody()))==5){
                    System.out.println("模拟发送消息失败");
                    int p = 1/0;
                }
                System.out.println(new Date().toString() + "," +"消息id:"+msg.getMsgId()+"，消息体:"+ new String(msg.getBody()) + "消费成功");
            }catch (Throwable e){
                //启用重试，return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;则表明不用重试
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        }
        // 返回消费状态：消费成功
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
});
```

注意：广播消费无重试功能！