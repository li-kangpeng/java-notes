### Ubuntu安装

#### 一、安装系统

###### **1**. 准备工具

*1.1* 一个8G以上的U盘 、软碟通UltralSO软件（https://cn.ultraiso.net/xiazai.html）

下载完后，双击安装，接受协议，勾选需要的附加任务，如下图

![图1](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/a69de2fc55544d278e32bb5adee54e57.png)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/062bdcbe59294040afb1dfcf25595909.png)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/7ba35f2adb9645efb349294948d75f90.png)

*1.2* ubuntu的镜像文件[ubuntu服务器版](https://ubuntu.com/download/server)（https://ubuntu.com/download/server）
在ubuntu官网下载ubuntu，点击Ubuntu Server 20.04LTS即可下载镜像文件

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/42b021f9f29b403590bbfdfb59cc36b4.png)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/5a0c641a7f6347839b2c7efa3ec9fe21.png)

这里安装的是服务器版，下载完成后是一个ISO镜像文件

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/151dfadaa0404b3bbb4bf5265b30bb0f.png)

*1.3* 打开UltraSO的刻录硬盘文件，选择继续试用，打开后在文件[下拉菜单](https://so.csdn.net/so/search?q=下拉菜单&spm=1001.2101.3001.7020)中打开Ubuntu的ISO文件，再选择任务栏 中的 启动->写入硬盘选项

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/9ac3b5966f3a403fa2435033000a7823.png)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/71ffbb6230b4423bb00724d203780e30.png)

保证硬盘驱动器是插入的U盘，影像文件是正确的I[SO文件](https://so.csdn.net/so/search?q=SO文件&spm=1001.2101.3001.7020)，写入方式是USB-HDD+，确认无误后，格式化U盘

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/6d9f38e4a57548e39fa712d8b3e788b3.png)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/38c80389b8594b91a49d73d235885bec.png)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/abe8fb3c74cf410095baddf8cba479fc.png)

刻录完成后，目录如下

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/7edd0984ed004eb69da27356f71d6132.png)

###### **2**. 装系统过程

将制作好的u盘做为启动盘，插入要装Ubuntu系统的电脑或者服务器，重启电脑后，按f12(不同电脑可能按键不同，自行查询)进入装机页面，选择Uefi的usb

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/fd83e8c8e2414ca283be52c4c487159e.jpeg)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/f0580a8d8daa439db1b141a8925dffbc.jpeg)

先不执行安装，按e修改文件内容,修改quiet 为nomodeset ,然后F10保存并退出到继续装环境的界面。

安装语言选择英语直接Done,键盘布局也直接默认，镜像源我这边设置的默认，也可以看自己需求设置其他比如阿里云镜像https://mirrors.aliyun.com/ubuntu/

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/36a9f82bc0dc4ca4844c11166536dd94.jpeg)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/59399420a68548af9265b12141c2768e.jpeg)

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/2835f8b04f1741acb85b42507776058a.jpeg)

设置镜像源后，选择不更新继续

![在这里插入图片描述](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/568dfce8440e4aea8d560b0a0bd90b19.jpeg)

3.![image-20231127151618905](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231127151618905.png)

#### 二、更新BIOS设置

如果不更新，系统则无法进行正常启动。

#### 三、设置ip

![image-20231127154354791](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231127154354791.png)

#### 四、给用户添加root权限

###### 1.先进行管理员账号的编辑，确保root权限的账户可直接进行登录。

![image-20231127154554654](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231127154554654.png)

实际上要想root权限用户直接登录，修改上面第一行即可。

生效：systemctl reload sshd.service

###### 2.编辑/etc/passwd文件

```shell
sudo vim /etc/passwd
```

将想要修改的那个用户名所对应的那一行修改，用户的UID更改为0

![image-20231127155408137](https://gitee.com/li-kangpeng/java-notes/raw/master/imgs/image-20231127155408137.png)