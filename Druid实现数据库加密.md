## 数据库密码加密

数据库密码直接写在配置中，对运维安全来说，是一个很大的挑战。可以使用`Druid`为此提供一种数据库密码加密的手段`ConfigFilter`。项目已经集成`druid`所以只需按要求配置即可。

#### 一、使用Druid加密数据库密码

##### 1.找到maven仓库com.alibaba.druid-spring-boot-starter下的druid的jar包

找到当前项目druid-spring-boot-starter的版本，找寻其下面的druid的jar包，如druid-1.2.4.jar。

##### 2、执行命令加密数据库密码

```sh
java -cp druid-1.2.4.jar com.alibaba.druid.filter.config.ConfigTools password
```

`password`输入你的数据库密码，输出的是加密后的结果。

```text
privateKey:MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAh9i5WSMFbg1Mnqbk6OzLtndnFaE8eDJK5ADvSc4/6Bf8Rae6HKXu6ycI+MoExiJo3L/ans0gpQz8xg1QRfbHNwIDAQABAkBYZLkWOyeJWLYGdep1db+IV2GIPYYvX+pDOih6Ce6jghkHNZpcpZKIFNseYCZlbDs0pkuHf7UE9TLcgyAfHc0hAiEA/82mfQxEkQIcs4GyEWk31T3DlAghoLO7CCKV3+YKJUsCIQCH83ZvdAie4P/w7axF1ltoqCxfSXMREl9U8YUlfU3uRQIhAPMINsqNskM3B3/AlCaYmdQ+RyfIhfKBcAvtmAlOLIt7AiBYR2OUo8glQv1vOSYwgy0AHJQTm+I4J7H3uo029tLv8QIhAICsFKCjzl5UR5fiz0yfObNTrtEoTiXH7ox8X/ORYKL8

publicKey:MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIfYuVkjBW4NTJ6m5Ojsy7Z3ZxWhPHgySuQA70nOP+gX/EWnuhyl7usnCPjKBMYiaNy/2p7NIKUM/MYNUEX2xzcCAwEAAQ==

password:GQ1JoSdsJ+KcPsWAq1cVibVk4t00AFUpYA6xzW9QoZ+downbRdyUMOA+i10qJL9kZhvZ6QqscjL2UoXg2jrWRw==
```

##### 3、配置数据源，提示`Druid`数据源需要对数据库密码进行解密。

###### （1）一般项目（配置没有使用dynamic）配置如下：

```yml
# 数据源配置
spring:
    datasource:
        type: com.alibaba.druid.pool.DruidDataSource
        driverClassName: com.mysql.cj.jdbc.Driver
        druid:
            # 主库数据源
            master:
                url: jdbc:mysql://localhost:3306/ry?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
                username: root
                password: GQ1JoSdsJ+KcPsWAq1cVibVk4t00AFUpYA6xzW9QoZ+downbRdyUMOA+i10qJL9kZhvZ6QqscjL2UoXg2jrWRw==
            # 从库数据源
            slave:
                # 从数据源开关/默认关闭
                enabled: false
                url: 
                username: 
                password: 
            # 初始连接数
            initialSize: 5
            # 最小连接池数量
            minIdle: 10
            # 最大连接池数量
            maxActive: 20
            # 配置获取连接等待超时的时间
            maxWait: 60000
            # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
            timeBetweenEvictionRunsMillis: 60000
            # 配置一个连接在池中最小生存的时间，单位是毫秒
            minEvictableIdleTimeMillis: 300000
            # 配置一个连接在池中最大生存的时间，单位是毫秒
            maxEvictableIdleTimeMillis: 900000
            # 配置检测连接是否有效
            validationQuery: SELECT 1 FROM DUAL
            testWhileIdle: true
            testOnBorrow: false
            testOnReturn: false
            connectProperties: config.decrypt=true;config.decrypt.key=MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIfYuVkjBW4NTJ6m5Ojsy7Z3ZxWhPHgySuQA70nOP+gX/EWnuhyl7usnCPjKBMYiaNy/2p7NIKUM/MYNUEX2xzcCAwEAAQ==
            webStatFilter: 
                enabled: true
            statViewServlet:
                enabled: true
                # 设置白名单，不填则允许所有访问
                allow:
                url-pattern: /druid/*
                # 控制台管理用户名和密码
                login-username: 
                login-password: 
            filter:
                config:
                    # 是否配置加密
                    enabled: true
                stat:
                    enabled: true
                    # 慢SQL记录
                    log-slow-sql: true
                    slow-sql-millis: 1000
                    merge-sql: true
                wall:
                    config:
                        multi-statement-allow: true
```

若有问题请参考若依文档：http://doc.ruoyi.vip/ruoyi/document/cjjc.html#%E9%9B%86%E6%88%90druid%E5%AE%9E%E7%8E%B0%E6%95%B0%E6%8D%AE%E5%BA%93%E5%AF%86%E7%A0%81%E5%8A%A0%E5%AF%86%E5%8A%9F%E8%83%BD

###### （2）配置中添加了dynamic时配置如下：

```yml
spring:
  datasource:
    druid:
      stat-view-servlet:
        enabled: true
        loginUsername: admin
        loginPassword: 123456
    dynamic:
      druid:
        initial-size: 5
        min-idle: 5
        maxActive: 20
        maxWait: 60000
        timeBetweenEvictionRunsMillis: 300000
        minEvictableIdleTimeMillis: 300000
        validationQuery: SELECT 1 FROM DUAL
        testWhileIdle: true
        testOnBorrow: false
        testOnReturn: false
        poolPreparedStatements: true
        maxPoolPreparedStatementPerConnectionSize: 20
        filters: stat,slf4j
        connectionProperties: druid.stat.mergeSql\=true;druid.stat.slowSqlMillis\=5000;log-slow-sql\=true
      datasource:
        # 主库数据源
        master:
          driver-class-name: com.mysql.cj.jdbc.Driver
          url: jdbc:mysql://192.168.5.189:3306/cloud_data_dev?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&allowMultiQueries=true&rewriteBatchedStatements=true
          username: root
          password: GQ1JoSdsJ+KcPsWAq1cVibVk4t00AFUpYA6xzW9QoZ+downbRdyUMOA+i10qJL9kZhvZ6QqscjL2UoXg2jrWRw==
          druid:
            public-key: MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIfYuVkjBW4NTJ6m5Ojsy7Z3ZxWhPHgySuQA70nOP+gX/EWnuhyl7usnCPjKBMYiaNy/2p7NIKUM/MYNUEX2xzcCAwEAAQ==
        # 从库数据源
        slave:
          username: root
          password: GQ1JoSdsJ+KcPsWAq1cVibVk4t00AFUpYA6xzW9QoZ+downbRdyUMOA+i10qJL9kZhvZ6QqscjL2UoXg2jrWRw==
          url: jdbc:mysql://192.168.5.189:3306/cloud_data_dev?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8&allowMultiQueries=true
          driver-class-name: com.mysql.cj.jdbc.Driver
          druid:
            public-key: MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIfYuVkjBW4NTJ6m5Ojsy7Z3ZxWhPHgySuQA70nOP+gX/EWnuhyl7usnCPjKBMYiaNy/2p7NIKUM/MYNUEX2xzcCAwEAAQ==
```

##### 4、启动应用程序测试验证加密结果

提示：如若忘记密码可以使用工具类解密（传入生成的公钥+密码）

```java
public static void main(String[] args) throws Exception
{
	String password = ConfigTools.decrypt(
"MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALizFQBZnHsPpj31Z8yOrrRL4R1jtrOnuEdW1Vt2vSKR/qRMqXjVeirWf8PT7srD33T8VuXzdwZpyhWVACDL1oUCAwEAAQ==",
			"gkYlljNHKe0/4z7bbJxD7v/txWJIFbiGWwsIPo176Q7fG0UjcSizNxuRUI2ll27ZPQf2ekiHFptus2/Rc4cmvA==");
	System.out.println("解密密码：" + password);
}
```

#### 二：使用jasypt加密数据库连接密码

##### 1、添加jasypt依赖

```text
<!-- 集成数据库密码加密工具jasypt  jdk8版本-->
    <dependency>
        <groupId>com.github.ulisesbocchio</groupId>
        <artifactId>jasypt-spring-boot-starter</artifactId>
        <version>1.14</version>
    </dependency>
```

##### 2、加密明文密码

我们编写一个加密方法对密码明文进行加密操作。代码如下（需要依赖jasypt-spring-boot-starter包）

```java
import org.jasypt.util.text.BasicTextEncryptor;

public class JasyptUtil {

    /**
    * 加密方法
    * @param salt 盐值
    * @param targetString 待加密字符串
    * @return 密文
    */
    public static String encrypt(String salt, String targetString) {
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(salt);
        return encryptor.encrypt(targetString);
    }

    /**
    * 解密方法
    * @param salt 盐值
    * @param targetString 待解密字符串
    * @return 明文
    */
    public static String decrypt(String salt,String targetString) {
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(salt);
        return encryptor.decrypt(targetString);
    }

    public static void main(String[] args) {
        String salt = "gjhcsw@2022";
        String password = "zh@2021";
        // 进行加密操作
        String encryptString1 = encrypt(salt, password);
        // 进行解密操作
        String decryptString1 = decrypt(salt, encryptString1);
        // 输出明文和密文
        System.out.println("encryptString1="+encryptString1);
        System.out.println("decryptString1="+decryptString1);
    }
}
```

运行main方法,输出如下：

```java
encryptString1=lQJcfvo4p5xRG26ZCXhW9A==
decryptString1=zh@2021
```

解释一下，假设我们的数据库密码明文为：zh@2021，盐值为：gjhcsw@2022，加密后的密文为：lQJcfvo4p5xRG26ZCXhW9A==

##### 3、更改数据库密码：

数据库密码改为ENC(lQJcfvo4p5xRG26ZCXhW9A==)即可

#### 4、配置加密盐和加密算法（三种实现方式）

###### （1）配置中添加盐和加密算法（盐被暴露，不推荐使用）：

```yaml
#加密盐值
jasypt:
  encryptor:
    #默认加密算法:PBEWITHHMACSHA512ANDAES_256，sha512+AES算法，安全性更高，但是需要 Java JDK 1.9+
    #本服务使用jdk1.8，所以使用 PBEWithMD5AndDES md5+des算法
    #默认使用 com.ulisesbocchio.jasyptspringboot.encryptor.DefaultLazyEncryptor 进行加解密 ，PooledPBEStringEncryptor可以对其加密的内容进行解密
    algorithm: PBEWithMD5AndDES
    # 加密密钥，使用方式 spring.datasource.password=ENC(密文)，不要设置在配置文件中，建议使用环境变量或者启动参数: --jasypt.encryptor.password=gjhcsw@2022
    password: gjhcsw@2022
    #设置密文前缀和后缀
    property:
      prefix: ENC(
      suffix: )
    iv-generator-classname: org.jasypt.iv.NoIvGenerator
```

最后我们还需要在main方法上添加注解开启自动加解密@EnableEncryptableProperties

注解 **@EnableEncryptableProperties** 即是开启自动加解密的关键。

注意：如果不使用password不是ENC()的话，使用原密码即可，即程序如果检测password不包含ENC()的话即不进行解密。

###### （2）启动时动态设置盐

a、本地调试需要在启动参数中配置JVM参数-DsecretKey=gjhcsw@2022

b、jar包启动命令 java -jar xxx.jar --jasypt.encryptor.password=gjhcsw@2022

在main方法上添加注解开启自动加解密@EnableEncryptableProperties

###### （3）使用代码设置盐和加密算法

```java
@Configuration
@EncryptablePropertySource("classpath:bootstrap.yml")
@EnableEncryptableProperties
//实现EncryptablePropertyResolver表明由此类进行解密
public class JasyptConfig implements EncryptablePropertyResolver {

    private static final String SALT = "gjhcsw@2022";

    private static final String KEY_OBTENTION_ITERATIONS = "1000";

    private static final String PREFIX = "ENC(";
    
    //加密bean
    @Bean(name = "jasyptStringEncryptor")
    public StringEncryptor stringEncryptor() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        // 这是加密的盐，建议直接硬编码，提高安全性
        config.setPassword(JasyptConfig.SALT);
        // 加密算法
        config.setAlgorithm("PBEWithMD5AndDES");
        // key 迭代次数
        config.setKeyObtentionIterations(JasyptConfig.KEY_OBTENTION_ITERATIONS);
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        // 随机盐生成器
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setStringOutputType("base64");
        encryptor.setConfig(config);
        return encryptor;
    }

    //解密bean
    @Bean(name = "encryptablePropertyResolver")
    public JasyptConfig encryptablePropertyResolver() {
        return new JasyptConfig();
    }

    //解密过程
    @Override
    public String resolvePropertyValue(String value) {
        StringEncryptor encryptor = SpringUtils.getBean("jasyptStringEncryptor");
        if (!StringUtils.isEmpty(value) && value.startsWith(JasyptConfig.PREFIX)) {
            String encValue = value.substring(JasyptConfig.PREFIX.length());
            return encryptor.decrypt(encValue);
        }
        return value;
    }

}
```

