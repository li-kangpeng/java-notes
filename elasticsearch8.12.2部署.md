## docker安装elasticsearch8.12.2

##### 1.镜像下拉

docker pull elasticsearch:8.12.2

##### 2.创建挂载路径

在eudcation目录下创建挂在路径es，es文件夹下创建plugins文件夹和config文件夹

[下载ik分词器](https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v8.12.2/elasticsearch-analysis-ik-8.12.2.zip)

进入plugins目录创建ik目录

```shell
mkdir ik
```

将压缩包解压到ik目录

```shell
unzip elasticsearch-analysis-ik-8.12.2.zip -d ik/
```

##### 3.启动容器

```js
docker run -d --restart=always --name es8 -p 9201:9200 -p 9301:9300 -v /home/education/es/plugins:/usr/share/elasticsearch/plugins -e "discovery.type=single-node" -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" elasticsearch:8.12.2
```

##### 4.将容器内部elasticsearch.yml文件拷贝到容器外

```sh
docker cp es8:/usr/share/elasticsearch/config/elasticsearch.yml /home/education/es/config
```

##### 5.修改elasticsearch.yml文件内容

```yml
cluster.name: "docker-cluster"
network.host: 0.0.0.0

#----------------------- BEGIN SECURITY AUTO CONFIGURATION -----------------------
#
# The following settings, TLS certificates, and keys have been automatically      
# generated to configure Elasticsearch security features on 28-02-2024 08:28:42
#
# --------------------------------------------------------------------------------

# Enable security features
xpack.security.enabled: true

xpack.security.enrollment.enabled: true

# Enable encryption for HTTP API client connections, such as Kibana, Logstash, and Agents
xpack.security.http.ssl:
  enabled: false
  keystore.path: certs/http.p12

# Enable encryption and mutual authentication between cluster nodes
xpack.security.transport.ssl:
  enabled: false
  verification_mode: certificate
  keystore.path: certs/transport.p12
  truststore.path: certs/transport.p12
#----------------------- END SECURITY AUTO CONFIGURATION -------------------------

```

##### 6.创建elasticsearch用户（因为源容器内用户为root分组下的elasticsearch用户）

```shell
sudo adduser elasticsearch
```

```shell
sudo usermod -aG sudo elasticsearch
```

##### 7.将修改完成的elasticsearch.yml文件拷贝进容器内

```shell
docker cp /home/education/es/config/elasticsearch.yml es8:/usr/share/elasticsearch/config/elasticsearch.yml
```

##### 8.重启容器

##### 9.修改es用户密码（es默认用户为elastic）

```shell
docker exec -it es8 /bin/bash
cd config/
elasticsearch-reset-password -u elastic
```

在输入y之后会输出新的密码

###### 10.进行连接