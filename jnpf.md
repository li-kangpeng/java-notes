邱常伟:
文档地址：https://jnpfsoft.coding.net/login
用户名密码：546184965@qq.com
Dhc@85996165

https://39.101.128.53:2243/dashboard/projects

git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-app.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-common.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-database.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-file-preview.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-java-cloud.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-resources.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-web.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-web-datareport-cloud.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-web-datascreen.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-web-electron.git
git clone https://546184965%40qq.com:Dhc%4085996165@e.coding.net/jnpfsoft/jnpf/jnpf-web-tenant.git



```
git clone 使用用户名和密码
一般git仓库的用户，都是用户名和密码登录，git clone命令如下
模板 git clone http://邮箱(或用户名):密码@仓库

git clone http://username:password@ip:port/xx/uem-vis-realtime.git

邮箱用户，git clone需要时注意如下：
注意： 邮箱中的 @ 要使用 %40 代替。仓库 URL 不需要 http://
示例：

邮箱 xw@qq.com
密码: xw
仓库: http://git.test.com/abc/demo

命令：

git clone http://xw%40qq.com:xw@git.test.com/abc/demo
```

```bash
# 在windows系统上使用gitbash克隆代码时提示如下
$ git clone https://39.101.128.53:2243/framework/jnpf-common.git
Cloning into 'jnpf-common'...
fatal: unable to access 'https://39.101.128.53:2243/framework/jnpf-common.git/': SSL certificate problem: self signed certificate

分析原因是git仓库https是自签证书，git访问的时候因为证书问题，不能访问代码仓库

问题解决：

用git自带的配置命令，设置sslVerify为false

git config --global http.sslVerify false
```

```
1）setting文件中mirrors节点里配置镜像

<mirror>
    <id>nexus</id>
    <mirrorOf>central</mirrorOf>
    <name>nexus</name>
    <url>http://XX/</url>
</mirror>
2）pom文件配置repositories、distributionManagement

<repositories>
    <repository>
        <id>XX-releases</id>
        <name>XX-releases</name>        
        <url>http://XX/</url>
        <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
        </releases>
        <snapshots>
            <enabled>false</enabled>
            <updatePolicy>always</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
        </snapshots>
    </repository>

    <repository>
        <id>XX-snapshots</id>
        <name>XX-snapshots</name>        
        <url>http://XX/</url>
        <releases>
            <enabled>false</enabled>
            <updatePolicy>never</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
        </releases>
        <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
        </snapshots>
    </repository>
</repositories>
<distributionManagement>
    <snapshotRepository>
        <id>XX-snapshots</id>
        <name>XX-snapshots</name>
        <url>http://XX/</url>
    </snapshotRepository>
    <repository>
        <id>XX-releases</id>
        <name>XX-releases</name>
        <url>http://XX/</url>
    </repository>
</distributionManagement>
其中：

</repositories>块----为从私服下载依赖jar包的配置，
<distributionManagement>块---为本项目打成的jar包上传至私服的配置。

3）如果下载或者上传时有权限验证，类似账号密码，可以在setting文件中的servers块中配置server

    <server>
    	<id>XX-releases</id>
    	<username>XX</username>
    	<password>XX</password>
    </server>
    <server>
    	<id>XX-snapshots</id>
    	<username>XX</username>
    	<password>XX</password>
    </server>
server中的id 与配置在pom文件中的repository id一样，这里只配置了上传jar包至私服的权限验证。

4）pom文件配置的仓库地址优先级高于setting文件中配置的仓库地址。

5）更改完成后，因为IDEA缓存等原因，会导致依赖爆红。没关系，如果远程仓库有，但是本地还是下载不下来，可以将本地仓库删除，重新下载。

然后试着执行clean package命令
```

## [maven](https://so.csdn.net/so/search?q=maven&spm=1001.2101.3001.7020)私服上传和下载包

一、上传
1、在maven安装包里面的settings.[xml](https://so.csdn.net/so/search?q=xml&spm=1001.2101.3001.7020)文件里面添加如下配置：

```xml
<server>
    <id>release</id>
    <username>用户名</username>
    <password>密码</password>
</server>

<server>
    <id>snapshot</id>
    <username>用户名</username>
    <password>密码</password>
</server>
```

这里的username和password分别代表你搭建的maven私服的用户名和密码

2、在要上传的模块里面的pom.xml文件里面添加如下配置：

```xml
<distributionManagement>
   <repository>
       <id>release</id>
       <name>Nexus Release Repository</name>
       <url>http://121.4.56.246:8099/repository/hapi-host-release/</url>
   </repository>

   <snapshotRepository>
       <id>snapshot</id>
       <name>Nexus Snapshot Repository</name>
       <url>http://121.4.56.246:8099/repository/hapi-host-snapshot/</url>
   </snapshotRepository>
</distributionManagement>

```

这里的id分别对应着上面settings.xml里面server对应的id(id必须要一致)，将jar上传到指定的库（hapi-host-release、hapi-host-snapshot）中

3、在idea项目中lifecycle -> deploy，通过deploy插件可以将jar上传到maven私服中。

二、下载
1、在maven安装包里面的settings.xml文件里面添加如下配置：

```xml
<!-- 下载jar包配置 -->
<profile> 
   <!--profile的id -->
   <id>dev</id>
   <repositories>
     <repository> <!--仓库id，repositories可以配置多个仓库，保证id不重复 -->
       <id>nexus</id> <!--仓库地址，即nexus仓库组的地址 -->
       <url>http://121.4.56.246:8099/repository/hapi-host-release/</url> <!--是否下载releases构件 -->
       <releases>
         <enabled>true</enabled>
       </releases> <!--是否下载snapshots构件 -->
       <snapshots>
         <enabled>true</enabled>
       </snapshots>
     </repository>
   </repositories>
   <pluginRepositories> <!-- 插件仓库，maven的运行依赖插件，也需要从私服下载插件 -->
     <pluginRepository> <!-- 插件仓库的id不允许重复，如果重复后边配置会覆盖前边 -->
       <id>public</id>
       <name>Public Repositories</name>
       <url>http://121.4.56.246:8099/repository/hapi-host-release/</url>
     </pluginRepository>
   </pluginRepositories>
</profile>

```

以上配置还不算完，还必须添加以下配置进行激活

```java
<activeProfiles>
   <activeProfile>dev</activeProfile>    
 </activeProfiles>
```

这里的dev代表上面profile里面的id