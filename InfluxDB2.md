## InfluxDB2

1.引入依赖

```java
    <dependency>
        <groupId>com.influxdb</groupId>
        <artifactId>influxdb-client-java</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
    </dependency>
```
2.配置文件

```yaml
  influx:
    url: http://192.168.1.102:8086
    token: xF2FXNMETEDKU-8_0U5Jlxki2mxxxoGtfb4HQglUOFjc1od9jWzv3Bqeqnmw0v33ItmGA-hW2J1PxKQw==
    org: my-org
    bucket: my-bucket
    database: testinfluxdb
```
3.config配置文件

```java
@Configuration
public class InfluxdbConfig {
    @Value("${spring.influx.url}")
    private String influxDBUrl;
    @Value("${spring.influx.token}")
    private String token;

    @Bean
    public InfluxDBClient influxDBClient() {
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create(influxDBUrl, token.toCharArray());
        influxDBClient.setLogLevel(LogLevel.BASIC);
        return influxDBClient;
    }
}
```

4.插入数据

```java
/**
 * 存入数据至时序数据库
 *
 * @param org         组织
 * @param bucket      存储桶
 * @param measurement 相当于MySQL的表
 * @param fields      相当于MySQL一条记录
 */
public void insertData(InfluxDBClient influxDBClient, String org, String bucket, String measurement, Map<String, Object> fields) {
    WriteOptions writeOptions = WriteOptions.builder()
            .batchSize(5000)
            .flushInterval(1000)
            .bufferLimit(10000)
            .jitterInterval(1000)
            .retryInterval(5000)
            .build();
    try (WriteApi writeApi = influxDBClient.getWriteApi(writeOptions)) {
        Point point = Point
                .measurement(measurement)
                .addFields(fields)
                .time(Instant.now(), WritePrecision.NS);
        writeApi.writePoint(bucket, org, point);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```

5.从influxDB读取数据

```java
 //从时序数据库查值
 Map<String, List<Map<String, Object>>> stringListMap = selectService.selectData(nameList);

@Service
public class SelectServiceImpl implements SelectService {
    @Value("${spring.influx.org}")
    private String org;

    @Value("${spring.influx.bucket}")
    private String bucket;

    @Resource
    private InfluxDBClient influxDBClient;

    private static final String MEASUREMENT = "environment_test";

    @Override
    public Map<String, List<Map<String, Object>>> selectData(List<String> stringList) {
        StringBuilder sql1 = new StringBuilder();
        for (int i = 0; i < stringList.size(); i++) {
            if (i == stringList.size() - 1) {
                sql1.append('"').append(stringList.get(i)).append('"');
            } else {
                sql1.append('"').append(stringList.get(i)).append('"').append(" or r[\"_field\"] == ");
            }
        }
        String sql2 = "|> range(start: -1m)";

        String sql = "from(bucket: \"%s\")\n" + sql2 +
                    "  |> filter(fn: (r) => r[\"_measurement\"] == \"%s\")\n" +
                    "  |> filter(fn: (r) => r[\"_field\"] == " + sql1 + ")\n" +
                    "  |> sort(columns:[\"valueTime\"])  " +
                    "  |> yield()";

        return select(sql);
    }

    @Override
    public Map<String, List<Map<String, Object>>> selectHistoryData(List<String> stringList) {
        StringBuilder sql1 = new StringBuilder();
        for (int i = 0; i < stringList.size(); i++) {
            if (i == stringList.size() - 1) {
                sql1.append('"').append(stringList.get(i)).append('"');
            } else {
                sql1.append('"').append(stringList.get(i)).append('"').append(" or r[\"_field\"] == ");
            }
        }
        String sql2 = "|> range(start: -23h)";
        String sql = "  from(bucket: \"%s\")\n" + sql2 +
                    "  |> filter(fn: (r) => r[\"_measurement\"] == \"%s\")\n" +
                    "  |> filter(fn: (r) => r[\"_field\"] == " + sql1 + ")\n" +
                    "  |> sort(columns:[\"valueTime\"])  " +
                    "  |> aggregateWindow(every: 1h, fn: mean, createEmpty: true)\n" +
                    "  |> yield(name: \"mean\")";

        return select(sql);
    }

    private Map<String, List<Map<String, Object>>> select(String sql){
        String flux = String.format(sql, bucket, MEASUREMENT);
        QueryApi queryApi = influxDBClient.getQueryApi();

        List<FluxTable> tables = queryApi.query(flux, org);

        Map<String, List<Map<String, Object>>> resulMap = new HashMap<>();

        if (CollectionUtil.isNotEmpty(tables)) {
            for (FluxTable table : tables) {
                List<FluxRecord> records = table.getRecords();
                List<Map<String, Object>> mapList = new ArrayList<>();
                for (FluxRecord fluxRecord : records) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("value", fluxRecord.getValue());
                    map.put("valueTime", fluxRecord.getTime());//Instant
                    map.put("field", fluxRecord.getField());
                    mapList.add(map);
                }
                String name = (String) mapList.get(0).get("field");
                resulMap.put(name, mapList);
            }
        }
        return resulMap;
}
```

注意：一般用sql为下面这种方式（_value为数字的情况）

```java
String sql = "  from(bucket: \"%s\")\n" + sql2 +
        "  |> filter(fn: (r) => r[\"_measurement\"] == \"%s\")\n" +
        "  |> filter(fn: (r) => r[\"_field\"] == " + sql1 + ")\n" +
        "  |> map(fn:(r) => ({ r with _value: float(v: r._value) }))" +
        "  |> sort(columns:[\"valueTime\"])  " +
        "  |> aggregateWindow(every: " + circle + ", fn: mean, createEmpty: true)\n" +
        "  |> yield(name: \"mean\")" +
        "  |> timeShift(duration: 8h) ";
```

|> map(fn:(r) => ({ r with _value: float(v: r._value) }))为将_value映射为float类型，不然无法使用聚合函数计算平均值。

|> timeShift(duration: 8h) "; 一般查询结果有八小时时差，使用这个会校准时差。