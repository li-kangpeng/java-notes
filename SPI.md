### SPI机制

##### 概念：

SPI（Service Provider Interface）技术是Java平台中一种用于实现可插拔、可扩展组件的机制。它允许开发者定义一组接口（Service Interface），然后通过SPI机制来动态地加载和使用不同的实现。

##### 原理：

SPI的基本原理如下：

1. 定义接口：首先，开发者需要定义一个接口，该接口定义了一组功能或服务。
2. 提供实现：接着，开发者可以提供多个实现该接口的类，每个类都提供了不同的功能或服务。
3. 配置文件：接下来，在类路径下创建一个特定的配置文件（约定为`META-INF/services/接口全限定名`），该文件的内容是实现类的全限定名，每行一个实现类。
4. 加载实现：通过Java的SPI机制，运行时会根据配置文件中的实现类信息，动态地加载并实例化这些实现类。
5. 使用服务：最后，开发者可以通过接口来访问已加载的实现类，使用提供的功能或服务。

##### 示例：

假设我们有一个名为`Logger`的接口，用于日志记录：

```java
public interface Logger {
    void log(String message);
}
```

然后我们提供两个实现类，分别用于不同的日志记录方式：

```java
public class ConsoleLogger implements Logger {
    @Override
    public void log(String message) {
        System.out.println("Console Logger: " + message);
    }
}

public class FileLogger implements Logger {
    @Override
    public void log(String message) {
        // 实现文件日志记录的逻辑
    }
}
```

接下来，我们在`src/main/resources`目录下创建一个名为`META-INF/services`的文件夹，然后在该文件夹中创建一个名为`com.example.Logger`的文件（请注意文件名必须为接口的全限定名称，内容是实现类的全限定名称）。文件的内容如下：

```tex
com.example.ConsoleLogger
com.example.FileLogger
```

这个文件指定了实现类的全限定名，每行一个。

最后，我们可以编写一个测试类来加载并使用这些实现类：

```java
import java.util.ServiceLoader;

public class Main {
    public static void main(String[] args) {
        ServiceLoader<Logger> loggerLoader = ServiceLoader.load(Logger.class);
        for (Logger logger : loggerLoader) {
            logger.log("Hello, SPI!");
        }
    }
}
```

运行该测试类，将会依次输出"Console Logger: Hello, SPI!"和"File Logger: Hello, SPI!"，分别对应两个实现类的日志记录方式。